import ky from 'ky'

const { VITE_API } = import.meta.env

/**
 * возвращает header для запроса
 */

const getHeaders = (userToken: string | null) => {
  const headers = {
    Authorization: <string | undefined>undefined,
  }
  if (userToken) {
    headers.Authorization = `Bearer ${userToken}`
  }
  return headers
}

/**
 * класс api
 */
export const apiClient = {
  /**
   * токен юзера
   */
  userToken: <string | null>null,

  /**
   * проставить токен юзера
   * @param {String} token
   */
  setUserToken: (token: string | null) => {
    apiClient.userToken = token
    return apiClient
  },

  get: (path: string) => ky.get(path, { prefixUrl: VITE_API, headers: getHeaders(apiClient.userToken) }),

  post: (path: string, data: Record<string, any>) =>
    ky.post(path, { json: data, prefixUrl: VITE_API, headers: getHeaders(apiClient.userToken) }),

  patch: (path: string, data: Record<string, any>) =>
    ky.patch(path, { json: data, prefixUrl: VITE_API, headers: getHeaders(apiClient.userToken) }),

  delete: (path: string, data: Record<string, any>) =>
    ky.delete(path, { json: data, prefixUrl: VITE_API, headers: getHeaders(apiClient.userToken) }),
}
