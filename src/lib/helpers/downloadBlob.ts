type downloadBlobType = (blob: Blob, fileName: string) => void

export const downloadBlob: downloadBlobType = (blob, fileName) => {
  const link = window.URL.createObjectURL(blob)
  const tempLink = document.createElement('a')
  tempLink.href = link
  tempLink.setAttribute('download', fileName)
  tempLink.click()
  tempLink.remove()
}
