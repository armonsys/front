export const useLocalStorage = <T, K>(key: string, initialValue: K): [() => T | K, (value: T) => void] => {
  const getValue = (): T | K => {
    try {
      const json = window.localStorage.getItem(key)
      if (!json) {
        return initialValue
      }
      const item: T = JSON.parse(json)
      if (item) {
        return item
      } else {
        return initialValue
      }
    } catch (error) {
      console.error(error)
      return initialValue
    }
  }

  const setValue = (value: T): void => {
    try {
      window.localStorage.setItem(key, JSON.stringify(value))
    } catch (error) {
      console.error(error)
    }
  }
  return [getValue, setValue]
}
