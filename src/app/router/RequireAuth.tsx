import { useAuth } from '@app/pages/Auth/store/store'
import { useLocation, Navigate } from 'react-router-dom'

import { ProfilePermissions } from '@pages/Auth/api/api'
import { toast } from 'react-toastify'

interface IRequireAuthProps {
  componentPerm?: keyof ProfilePermissions
  children: JSX.Element
}

export const RequireAuth = ({ componentPerm, children }: IRequireAuthProps) => {
  const profile = useAuth((state) => state.profile)
  const location = useLocation()

  if (!profile) {
    return <Navigate to='/auth' state={{ from: location }} replace />
  }

  if (componentPerm && !profile[componentPerm]) {
    toast.error('У вас нет доступа к этой странице')
    return <Navigate to='/' replace />
  }

  return children
}
