import { Layout } from '@consta/uikit/Layout'
import { Route, Routes } from 'react-router-dom'
import './App.css'
import { HeaderLayout } from '@app/layouts/HeaderLayout/HeaderLayout.tsx'
import Auth from './pages/Auth/Auth.tsx'
import { RequireAuth } from './router/RequireAuth.tsx'
import { lazy, Suspense, useLayoutEffect, useState } from 'react'
import { Loader } from '@consta/uikit/Loader/index'
import style from './App.module.scss'
import { EventModal } from './components/EventModal/EventModal.tsx'
import { apiClient } from '@clients/apiClient.ts'
import { useLocalStorage } from '@lib/hooks/useLocalStorage.ts'
import { getProfile, refreshAccessToken } from './pages/Auth/api/api.ts'
import { useAuth } from './pages/Auth/store/store.ts'

const Map = lazy(() => import('@pages/Map/Map'))
const Dashboard = lazy(() => import('@pages/Dashboard/Dashboard'))

const App = () => {
  const [getJwtRefresh] = useLocalStorage<string, null>('JWTRefresh', null)
  const [profile, setProfile] = useAuth((state) => [state.profile, state.setProfile])

  const [isLoading, setIsLoading] = useState<boolean>(true)

  useLayoutEffect(() => {
    if (profile) return
    const refreshTokenHandler = async () => {
      try {
        const refreshToken = getJwtRefresh()
        if (refreshToken) {
          const refreshResponse = await refreshAccessToken({ token: refreshToken })
          if (refreshResponse.token) {
            apiClient.setUserToken(refreshResponse.token)
            const userProfileResp = await getProfile()
            setProfile(userProfileResp)
          }
        }
      } catch (e) {
        localStorage.removeItem('JWTRefresh')
      } finally {
        setIsLoading(false)
      }
    }

    refreshTokenHandler()
  }, [])

  return (
    <Layout direction='column' style={{ maxHeight: '100vh', height: '100vh' }}>
      {isLoading ? (
        <div className={style.loaderContainer}>
          <Loader />
        </div>
      ) : (
        <>
          <HeaderLayout />
          <Suspense
            fallback={
              <div className={style.loaderContainer}>
                <Loader />
              </div>
            }
          >
            <Routes>
              <Route
                path='/*'
                element={
                  <RequireAuth>
                    <Dashboard />
                  </RequireAuth>
                }
              />
              <Route
                path='/map'
                element={
                  <RequireAuth componentPerm='can_view_map'>
                    <Map />
                  </RequireAuth>
                }
              />
              <Route path='/auth' element={<Auth />} />
            </Routes>
            <EventModal />
          </Suspense>
        </>
      )}
    </Layout>
  )
}

export default App
