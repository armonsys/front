import { shallow } from 'zustand/shallow'
import { createWithEqualityFn } from 'zustand/traditional'

export type Item = {
  label: string
  id: number
}

interface ObjectsSelectState {
  objectsArr: Item[]
  chosenConstruct: Item | null
  setChosenConstruct: (construct: Item | null) => void
  setObjectsArr: (arr: Item[]) => void
}

export const useObjectsSelect = createWithEqualityFn<ObjectsSelectState>(
  (set) => ({
    objectsArr: [],
    chosenConstruct: null,
    setChosenConstruct: (construct) => set(() => ({ chosenConstruct: construct })),
    setObjectsArr: (arr) => set(() => ({ objectsArr: arr || [] })),
  }),
  shallow,
)
