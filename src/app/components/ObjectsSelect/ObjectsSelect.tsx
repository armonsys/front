import { FC } from 'react'
import style from './ObjectsSelect.module.scss'
import { Card } from '@consta/uikit/Card'
import { Select } from '@consta/uikit/Select'
import { Text } from '@consta/uikit/Text'
import { useObjectsSelect } from '@app/components/ObjectsSelect/store/store'

interface IObjectsSelectProps {}

export const ObjectsSelect: FC<IObjectsSelectProps> = () => {
  const [objectsArr, currentConstruct, setCurrentConstruct] = useObjectsSelect((state) => [
    state.objectsArr,
    state.chosenConstruct,
    state.setChosenConstruct,
  ])

  return (
    <Card className={style.wrapper}>
      <Text className={style.title} size='2xl' weight='bold'>
        Объект
      </Text>
      <Select
        items={objectsArr}
        value={currentConstruct}
        onChange={({ value }) => {
          setCurrentConstruct(value)
        }}
      />
    </Card>
  )
}
