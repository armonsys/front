import { JournalDataType } from '@app/pages/Dashboard/pages/Main/components/Table/store/store'
import { shallow } from 'zustand/shallow'
import { createWithEqualityFn } from 'zustand/traditional'

interface EventModalState {
  isOpen: boolean
  setIsOpen: (isOpen: boolean) => void
  modalContent: JournalDataType | undefined
  setModalContent: (modalContent: JournalDataType | undefined) => void
}

export const useEventModal = createWithEqualityFn<EventModalState>(
  (set) => ({
    isOpen: false,
    setIsOpen: (isOpen) => set(() => ({ isOpen })),
    modalContent: undefined,
    setModalContent: (modalContent) => set(() => ({ modalContent })),
  }),
  shallow,
)
