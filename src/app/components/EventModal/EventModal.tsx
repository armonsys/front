import { JournalDataType, useMainTable } from '@app/pages/Dashboard/pages/Main/components/Table/store/store'
import { Button } from '@consta/uikit/Button'
import { Modal } from '@consta/uikit/Modal'
import { Text } from '@consta/uikit/Text'
import { TextField } from '@consta/uikit/TextField'
import { FC, useEffect, useRef, useState } from 'react'
import style from './EventModal.module.scss'
import { useEventModal } from './store/store'
import { useMutation } from '@tanstack/react-query'
import { patchIncident } from '@app/pages/Dashboard/pages/Main/components/Table/api/api'
import { queryClient } from '@clients/queryClient'
import { toast } from 'react-toastify'
import { useAuth } from '@app/pages/Auth/store/store'

type ModalRowItem = {
  label: string
  accessor: keyof JournalDataType
  type?: 'date' | 'link' | 'object'
}

const modalRows: ModalRowItem[] = [
  {
    label: 'Дата регистрации события',
    accessor: 'created_at',
    type: 'date',
  },
  {
    label: 'Прогнозная дата события:',
    accessor: 'predicted_date',
    type: 'date',
  },
  {
    label: 'Объект',
    accessor: 'construct',
    type: 'object',
  },
  {
    label: 'Рекомендация',
    accessor: 'recommendations',
  },
  {
    label: 'Срок исполнения',
    accessor: 'done_at',
    type: 'date',
  },
  {
    label: 'Отчет',
    accessor: 'review_filename',
    type: 'link',
  },
]

interface IEventModalProps {}

export const EventModal: FC<IEventModalProps> = () => {
  const [isOpen, setIsOpen] = useEventModal((state) => [state.isOpen, state.setIsOpen])
  const [modalContent, setModalContent] = useEventModal((state) => [state.modalContent, state.setModalContent])
  const [rows, setRows] = useMainTable((state) => [state.rows, state.setRows])
  const profile = useAuth((state) => state.profile)

  const [textfieldValue, setTextfieldValue] = useState(modalContent?.comments || '')

  const inputRef = useRef<HTMLInputElement>(null)

  const { mutate } = useMutation({
    mutationFn: patchIncident,
    // When mutate is called:
    onMutate: async (newJournalItem) => {
      await queryClient.cancelQueries(['journal'])

      const previousJournalState = rows

      setRows(
        rows.map((item) => {
          return Number(item.id) === Number(newJournalItem.id)
            ? { ...newJournalItem, id: newJournalItem.id.toString() }
            : item
        }),
      )

      return { previousJournalState }
    },
    onError: (err, newTodo, context) => {
      console.error(err)
      toast.error('Ошибка при выполнении запроса')
      setRows(context?.previousJournalState || [])
    },
    onSettled: () => {
      queryClient.invalidateQueries(['journal'])
    },
  })

  const handleChangeTextField = ({ value }: { value: string | null }) => {
    if (modalContent?.comments) return
    setTextfieldValue(value || '')
  }

  const closeHandler = () => {
    setIsOpen(false)
    setTextfieldValue('')
    setModalContent(undefined)
  }

  const getDateTimeString = (dateString: unknown) => {
    if (typeof dateString === 'string') {
      const date = new Date(dateString)
      return `${date.toLocaleDateString('ru-Ru')} ${date.toLocaleTimeString('ru-Ru')}`
    }
    return ''
  }

  const completeClickHandler = async () => {
    if (!modalContent || !profile) return
    await mutate({
      ...modalContent,
      id: Number(modalContent.id),
      status: 'Выполнено',
      comments: textfieldValue,
      done_at: new Date().toISOString(),
      worker: {
        id: profile?.id,
        username: profile?.username,
      },
    })
    closeHandler()
  }

  const renderRowContent = (row: ModalRowItem) => {
    if (!modalContent && !modalContent?.[row.accessor]) return ''

    if (row.type === 'date') {
      return getDateTimeString(modalContent[row.accessor])
    } else if (row.type === 'link') {
      const rowValue = modalContent[row.accessor] as JournalDataType['review_filename']
      return rowValue ? (
        <a target='_blank' href={rowValue.url} rel='noreferrer'>
          {rowValue.name}
        </a>
      ) : (
        ''
      )
    } else if (row.type === 'object') {
      const rowValue = modalContent[row.accessor] as JournalDataType['construct']
      return rowValue.name
    } else {
      return modalContent[row.accessor] as string
    }
  }

  const handleAddFileClick = () => {
    if (!inputRef.current) return
    inputRef.current.click()
  }

  const handleFileChange = (event: any) => {
    if (!modalContent) return
    const fileObj = event.target.files && event.target.files[0]
    if (!fileObj) {
      return
    }
    const newRows = rows.map((row) => {
      if (row.id === modalContent?.id && modalContent?.status === 'Новое') {
        return {
          ...row,
          summary: {
            name: fileObj.name,
            url: URL.createObjectURL(fileObj),
          },
        }
      }
      return row
    })
    setRows(newRows)
    setModalContent({
      ...modalContent,
      review_filename: {
        name: fileObj.name as string,
        url: URL.createObjectURL(fileObj) as string,
      },
    })
    event.target.value = null
  }

  useEffect(() => {
    // mark as seen
    if (!modalContent || modalContent.status === 'Просмотрено' || modalContent.status === 'Выполнено') return
    mutate({ ...modalContent, id: Number(modalContent.id), status: 'Просмотрено' })
  }, [mutate, modalContent])

  useEffect(() => {
    if (modalContent?.comments) setTextfieldValue(modalContent?.comments)
  }, [modalContent?.comments])

  return (
    <Modal isOpen={isOpen} onClickOutside={closeHandler} onEsc={closeHandler} className={style.container}>
      {modalContent && (
        <>
          <header className={style.header}>
            <Text size='2xl' weight='bold'>
              {modalContent.type}
            </Text>
            <Button label='x' onClick={closeHandler} />
          </header>
          <div className={style.gridContainer}>
            {modalRows.map((row, idx) => {
              if (!modalContent[row.accessor]) return null
              return (
                <div className={style.gridRow} key={idx}>
                  <Text align='right' weight='bold'>
                    {row.label}
                  </Text>
                  <Text>{renderRowContent(row)}</Text>
                </div>
              )
            })}
            <div className={style.gridRow}>
              <Text align='right' weight='bold'>
                Комментарий исполнителя:
              </Text>
              <TextField
                disabled={modalContent?.status === 'Выполнено'}
                onChange={handleChangeTextField}
                value={textfieldValue}
                rows={5}
                type='textarea'
              />
            </div>
            <div className={style.btnsContainer}>
              <Button
                disabled={modalContent.status === 'Выполнено'}
                width='default'
                label='Выполнить'
                onClick={completeClickHandler}
              />
              <Button width='default' label='Загрузить отчет' onClick={handleAddFileClick} />
              <input style={{ display: 'none' }} ref={inputRef} type='file' onChange={handleFileChange} />
            </div>
          </div>
        </>
      )}
    </Modal>
  )
}
