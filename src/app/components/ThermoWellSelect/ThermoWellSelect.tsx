import { FC } from 'react'
import style from './ThermoWellSelect.module.scss'
import { Card } from '@consta/uikit/Card'
import { Select } from '@consta/uikit/Select'
import { Text } from '@consta/uikit/Text'
import { Item } from '../../pages/Dashboard/pages/KeyMonitoring/KeyMonitoring'

interface IThermoWellSelectProps {
  items: Item[]
  currWell: Item | null
  setCurrWell: React.Dispatch<React.SetStateAction<Item | null>>
}

export const ThermoWellSelect: FC<IThermoWellSelectProps> = ({ items, currWell, setCurrWell }) => {
  return (
    <Card className={style.wrapper}>
      <Text className={style.title} size='2xl' weight='bold'>
        Скважина
      </Text>
      <Select items={items} value={currWell} onChange={({ value }) => setCurrWell(value)} />
    </Card>
  )
}
