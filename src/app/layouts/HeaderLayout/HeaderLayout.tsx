import { FC, useRef, useState } from 'react'
import style from './HeaderLayout.module.scss'

import { useAuth } from '@app/pages/Auth/store/store'
import { ReactComponent as DigitalGtmLogo } from '@assets/digital_gtm_logo.svg'
import { ReactComponent as GpnLogo } from '@assets/gpn_logo.svg'
import { Header, HeaderMenu, HeaderModule } from '@consta/uikit/Header'
import { useLocation, useNavigate } from 'react-router-dom'

import { User } from '@consta/uikit/__internal__/src/components/User/User'
import { Button } from '@consta/uikit/__internal__/src/components/Button/Button'
import { Popover } from '@consta/uikit/Popover'
import { Card } from '@consta/uikit/Card'
import { apiClient } from '@clients/apiClient'
import { logout } from '@app/pages/Auth/api/api'

interface IHeaderLayoutProps {}

export const HeaderLayout: FC<IHeaderLayoutProps> = () => {
  const profile = useAuth((state) => state.profile)
  const navigate = useNavigate()
  const location = useLocation()
  const [exitPopup, setExitPopup] = useState(false)
  const anchorRef = useRef(null)

  const { pathname } = location
  const menuItems = [
    {
      label: 'Дашборд',
      active: pathname === '/',
      onClick: () => navigate('/'),
    },
    { label: 'Карта', active: pathname === '/map', onClick: () => navigate('/map'), access: 'can_view_map' as const },
  ].filter((item) => {
    return item.access ? profile && profile[item.access] : true
  })

  const exitHandler = async () => {
    const json = localStorage.getItem('JWTRefresh')
    const refreshToken = json ? JSON.parse(json) : null
    if (refreshToken) {
      await logout({ token: refreshToken })
    }
    apiClient.setUserToken('')
    localStorage.removeItem('JWTRefresh')
    navigate('/')
    window.location.reload()
  }

  return (
    <Header
      leftSide={
        <>
          <HeaderModule>
            <div className={style.logoContainer} onClick={() => navigate('/')}>
              <GpnLogo />
              <DigitalGtmLogo />
            </div>
          </HeaderModule>
          <HeaderModule>
            <HeaderMenu items={menuItems} />
          </HeaderModule>
        </>
      }
      rightSide={
        <HeaderModule>
          {!!profile && (
            <>
              <User
                className={style.login}
                name={profile?.username}
                withArrow
                onClick={() => setExitPopup((prev) => !prev)}
                ref={anchorRef}
              />
              {exitPopup && (
                <Popover
                  direction='downCenter'
                  spareDirection='downStartLeft'
                  offset='2xs'
                  arrowOffset={0}
                  onClickOutside={() => setExitPopup(false)}
                  isInteractive
                  anchorRef={anchorRef}
                  equalAnchorWidth={false}
                >
                  <Card className={style.popup}>
                    <Button size='s' view='clear' label='Выйти' onClick={exitHandler} />
                  </Card>
                </Popover>
              )}
            </>
          )}
        </HeaderModule>
      }
    />
  )
}
