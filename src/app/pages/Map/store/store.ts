import { shallow } from 'zustand/shallow'
import { createWithEqualityFn } from 'zustand/traditional'

interface MapState {
  layer: 'thermometricWells' | 'warpMarkers' | null
  setLayer: (layer: 'thermometricWells' | 'warpMarkers') => void
}
export const useMap = createWithEqualityFn<MapState>(
  (set) => ({
    layer: null,
    setLayer: (layer) => set(() => ({ layer })),
  }),
  shallow,
)
