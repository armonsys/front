import { FC } from 'react'
import style from './ZoomBox.module.scss'
import { Button } from '@consta/uikit/Button'
import { IconAdd } from '@consta/icons/IconAdd'
import { IconRemove } from '@consta/icons/IconRemove'

interface IZoomBoxProps {
  mapScale: number
  setMapScale: React.Dispatch<React.SetStateAction<number>>
}

export const ZoomBox: FC<IZoomBoxProps> = ({ setMapScale, mapScale }) => {
  return (
    <div className={style.container}>
      <Button
        className={style.button}
        iconRight={IconAdd}
        view='clear'
        onlyIcon
        form='round'
        disabled={mapScale >= 1}
        onClick={() => {
          setMapScale((prev) => prev + 0.05)
        }}
      />
      <Button
        className={style.button}
        iconRight={IconRemove}
        view='clear'
        onlyIcon
        form='round'
        disabled={mapScale <= 0.4}
        onClick={() => {
          setMapScale((prev) => prev - 0.05)
        }}
      />
    </div>
  )
}
