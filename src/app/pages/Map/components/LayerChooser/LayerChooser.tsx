import { IconLayers } from '@consta/icons/IconLayers'
import { Button } from '@consta/uikit/Button'
import { ContextMenu } from '@consta/uikit/ContextMenu'
import { Radio } from '@consta/uikit/Radio'
import { FC, useRef, useState } from 'react'
import style from './LayerChooser.module.scss'

import { useMap } from '../../store/store'
import { useAuth } from '@app/pages/Auth/store/store'
import { ProfilePermissions } from '@app/pages/Auth/api/api'

const groups = [{ key: 1, name: 'Слои' }]

type ItemType = {
  id: number
  label: string
  layer: string
  group: number
  access: keyof ProfilePermissions
}

const items: ItemType[] = [
  { id: 1, label: 'Термометрические скважины', layer: 'thermometricWells', group: 1, access: 'can_view_downhole' },
  { id: 2, label: 'Деформационные маркеры', layer: 'warpMarkers', group: 1, access: 'can_view_deformation' },
]

interface ILayerChooserProps {}

export const LayerChooser: FC<ILayerChooserProps> = () => {
  const profile = useAuth((state) => state.profile)
  const [currLayer, setLayer] = useMap((state) => [state.layer, state.setLayer])

  const [isOpen, setIsOpen] = useState(false)
  const ref = useRef<HTMLButtonElement>(null)

  const clickHandler = (item: any): any => {
    setLayer(item.layer)
  }

  const renderLeftSide = (isChecked: boolean): React.ReactNode => {
    return <Radio checked={isChecked} />
  }

  if (items.filter((predicate) => profile && profile?.[predicate.access]).length === 0) return null

  return (
    <div className={style.container}>
      <Button
        ref={ref}
        className={style.button}
        iconRight={IconLayers}
        onlyIcon
        form='round'
        onClick={() => setIsOpen((prev) => !prev)}
      />
      <ContextMenu
        getItemOnClick={clickHandler}
        title='Слои'
        size='xs'
        anchorRef={ref}
        isOpen={isOpen}
        items={items.filter((predicate) => profile && profile?.[predicate.access])}
        groups={groups}
        getItemLeftSide={(item) => renderLeftSide(item.layer === currLayer)}
        getItemGroupId={(item) => item.group}
        getGroupId={(group) => group.key}
        getGroupLabel={(group) => group.name}
        onClickOutside={() => setIsOpen(false)}
      />
    </div>
  )
}
