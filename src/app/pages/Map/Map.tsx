import greenDot from '@assets/green_dot.svg'
import map from '@assets/map.png'
import redTriangle from '@assets/red_triangle.svg'
import snowIcon from '@assets/snow_icon.svg'
import { Layout } from '@consta/uikit/Layout'
import { Loader } from '@consta/uikit/Loader/index'
import { FC, useEffect, useMemo, useRef, useState } from 'react'
import ScrollContainer from 'react-indiana-drag-scroll'
import { useNavigate } from 'react-router-dom'

import style from './Map.module.scss'
import { LayerChooser } from './components/LayerChooser/LayerChooser.tsx'
import { ZoomBox } from './components/ZoomBox/ZoomBox.tsx'
import { ClickableAreaType, LayerType, thermometricWellsLayer, warpMarkers } from './layersMockData.ts'
import { useMap } from './store/store.ts'
import { Tooltip } from '@consta/uikit/Tooltip'
import { Text } from '@consta/uikit/Text'
import { useEventModal } from '@app/components/EventModal/store/store.ts'
import { useMainTable } from '../Dashboard/pages/Main/components/Table/store/store.ts'
import { useAuth } from '../Auth/store/store.ts'
import { useQuery } from '@tanstack/react-query'
import { fetchJournal } from '../Dashboard/pages/Main/components/Table/api/api.ts'

interface IMapProps {}

const Map: FC<IMapProps> = () => {
  const navigate = useNavigate()

  const [layer, setLayer] = useMap((state) => [state.layer, state.setLayer])
  const setIsEventModalOpen = useEventModal((state) => state.setIsOpen)
  const setModalContent = useEventModal((state) => state.setModalContent)
  const rows = useMainTable((state) => state.rows)
  const profile = useAuth((state) => state.profile)

  const scrollRef = useRef<HTMLElement>(null)
  const canvasObjectsRef = useRef<HTMLCanvasElement>(null)
  const canvasLayerRef = useRef<HTMLCanvasElement>(null)
  const imgContainerRef = useRef<HTMLDivElement>(null)
  const imgRef = useRef<HTMLImageElement>(null)

  const [canvasSize, setCanvasSize] = useState({ w: 0, h: 0 })
  const [areas, setAreas] = useState<ClickableAreaType[]>([])
  const [mapScale, setMapScale] = useState(1)
  const [imgLoader, setImgLoader] = useState(true)
  const [sensors, setSensors] = useState<ClickableAreaType['sensors']>([])
  const [thermoStableSensors, setThermoStableSensors] = useState<ClickableAreaType['thermostableSensor'][]>([])
  const [position, setPosition] = useState<{ x: number; y: number } | undefined>(undefined)
  const [isTooltipVisible, setIsTooltipVisible] = useState(false)
  const [tooltipText, setTooltipText] = useState('')

  const { data: journalData, isLoading } = useQuery({
    queryKey: ['journal'],
    queryFn: () => fetchJournal(),
    enabled: profile?.can_view_journal,
  })

  const getSensorWithEvent = (e: React.MouseEvent<HTMLImageElement, MouseEvent>) => {
    const rect = e.currentTarget.getBoundingClientRect()
    const x = e.clientX - rect.left
    const y = e.clientY - rect.top
    const sensorWithEvent = sensors.find((sensor) => {
      return Math.abs(sensor.x - x) <= 15 && Math.abs(sensor.y - y) <= 15
    })
    return sensorWithEvent
  }

  const getThermostableSensor = (e: React.MouseEvent<HTMLImageElement, MouseEvent>) => {
    const rect = e.currentTarget.getBoundingClientRect()
    const x = e.clientX - rect.left
    const y = e.clientY - rect.top
    const thermostableSensorWithEvent = thermoStableSensors.find((sensor) => {
      if (!sensor) return false
      return Math.abs(sensor.x - x) <= 15 && Math.abs(sensor.y - y) <= 15
    })
    return thermostableSensorWithEvent
  }

  const mouseClickHandler = (e: React.MouseEvent<HTMLImageElement, MouseEvent>) => {
    const sensorWithEvent = getSensorWithEvent(e)
    if (sensorWithEvent) {
      const sensorEvent = rows.find(
        (row) =>
          row.construct.id === sensorWithEvent.constructId &&
          row.type === 'Устройство не в сети' &&
          row.status !== 'Выполнено',
      )
      navigate('/')
      setIsEventModalOpen(true)
      setModalContent(sensorEvent)
    }
  }

  const mouseMoveHandler = (e: React.MouseEvent<HTMLImageElement, MouseEvent>) => {
    setPosition({ x: e.clientX, y: e.clientY })
    const sensorWithEvent = getSensorWithEvent(e)
    const thermostableSensor = getThermostableSensor(e)
    const isSensorHasEvent = journalData?.find(
      (event) =>
        event.construct.id === sensorWithEvent?.constructId &&
        event.type === 'Устройство не в сети' &&
        event.status !== 'Выполнено',
    )
    if (sensorWithEvent && !!isSensorHasEvent) {
      setIsTooltipVisible(true)
      if (!isSensorHasEvent.created_at) return
      setTooltipText(`Устройство не в сети с ${new Date(isSensorHasEvent.created_at).toLocaleDateString('ru-RU')}`)
    } else if (thermostableSensor) {
      setIsTooltipVisible(true)
      setTooltipText('Установлено 4 системы ТСГ типа ГЕТ')
    } else {
      setIsTooltipVisible(false)
      setTooltipText('')
    }
  }

  const mouseEnterHandler = (build: ClickableAreaType) => {
    if (!canvasObjectsRef.current) return
    const ctx = canvasObjectsRef.current.getContext('2d')
    if (!ctx) return
    const coords = build.coords.split(',').map((coord) => Number(coord))
    // calculate canvas coords from area coords
    const x = coords[0]
    const y = coords[1]
    const width = coords[2] - coords[0]
    const height = coords[3] - coords[1]
    //
    ctx.fillStyle = 'rgba(0, 120, 210, 0.4)'
    ctx.fillRect(x, y, width, height)
  }

  const mouseLeaveHandler = () => {
    if (!canvasObjectsRef.current) return
    const ctx = canvasObjectsRef.current.getContext('2d')
    ctx?.clearRect(0, 0, canvasObjectsRef.current?.width, canvasObjectsRef.current?.height)
  }

  const clickHandler = (build: ClickableAreaType) => {
    if (layer === 'warpMarkers') {
      navigate(`/geo_monitoring?id=${build.id}`)
    } else if (layer === 'thermometricWells') {
      navigate(`/key_monitoring?id=${build.id}`)
    }
  }

  const drawLayer = (imgUrl: string, layer: LayerType, canvasNode: HTMLCanvasElement | null) => {
    if (!canvasNode?.width || !canvasNode?.height) {
      return
    }
    setAreas(layer.areas)
    if (!canvasNode) return
    const ctx = canvasNode.getContext('2d')
    if (!ctx) return
    ctx?.clearRect(0, 0, canvasNode.width, canvasNode.height)
    const allSensors = layer.areas.reduce((acc: ClickableAreaType['sensors'], area) => {
      return [...acc, ...area.sensors.map((sensor) => ({ ...sensor, constructId: area.id }))]
    }, [])

    setSensors(allSensors)
    const img = new Image()
    img.src = imgUrl
    img.onload = () => {
      allSensors.forEach((sensor) => {
        ctx.save()
        ctx.translate(sensor.x, sensor.y)
        if (sensor.rotate) {
          ctx.rotate(sensor.rotate * (Math.PI / 180))
        }
        ctx.drawImage(img, -img.width / 2, -img.height / 2)
        if (
          journalData?.find(
            (event) =>
              event.construct.id === sensor.constructId &&
              event.type === 'Устройство не в сети' &&
              event.status !== 'Выполнено',
          )
        ) {
          ctx.beginPath()
          ctx.arc(0, 0, 12, 0, 2 * Math.PI)
          ctx.strokeStyle = '#ff4443'
          ctx.lineWidth = 2
          ctx.stroke()
          ctx.fillStyle = 'rgba(255, 68, 67, 0.6)'
          ctx.fill()
        }
        ctx.restore()
      })
    }
    const thermostableSensors = layer.areas.reduce((acc: ClickableAreaType['thermostableSensor'][], area) => {
      if (area.thermostableSensor) {
        return [...acc, area.thermostableSensor]
      } else {
        return acc
      }
    }, [])
    if (thermostableSensors.length > 0) {
      setThermoStableSensors(thermostableSensors)
      const img = new Image()
      img.src = snowIcon
      img.onload = () => {
        thermostableSensors.forEach((sensor) => {
          if (!sensor) return
          ctx.save()
          ctx.translate(sensor.x, sensor.y)
          ctx.drawImage(img, -img.width / 2, -img.height / 2)
          ctx.restore()
        })
      }
    }
  }

  const drawFunctions = useMemo(() => {
    return {
      thermometricWells: () => drawLayer(greenDot, thermometricWellsLayer, canvasLayerRef.current),
      warpMarkers: () => drawLayer(redTriangle, warpMarkers, canvasLayerRef.current),
    }
  }, [canvasLayerRef, journalData])

  const scrollImgToCenter = () => {
    if (!scrollRef.current) return
    const scrollContainerSize = scrollRef.current?.getBoundingClientRect()
    const imgContainerSize = imgContainerRef.current?.getBoundingClientRect()
    if (!scrollContainerSize || !imgContainerSize) return
    scrollRef.current?.scrollTo({
      left: Math.abs(imgContainerSize.width / 2 - scrollContainerSize.width / 2),
      top: Math.abs(imgContainerSize.height / 2 - scrollContainerSize.height / 2),
    })
  }

  useEffect(() => {
    const imgNode = imgRef.current
    const imgResizeObserver = new ResizeObserver((entries) => {
      const img = entries[0].target
      const imgSize = img.getBoundingClientRect()
      setCanvasSize({ w: imgSize.width, h: imgSize.height })
      scrollImgToCenter()
    })
    if (imgNode) {
      imgResizeObserver.observe(imgNode)
    }

    return () => {
      imgResizeObserver.disconnect()
    }
  }, [imgRef, drawFunctions])

  useEffect(() => {
    if (!canvasSize.w || !canvasSize.h || !layer) return
    drawFunctions[layer]()
  }, [canvasSize, layer, drawFunctions])

  useEffect(() => {
    if (profile?.can_view_downhole) {
      setLayer('thermometricWells')
    } else if (profile?.can_view_deformation) {
      setLayer('warpMarkers')
    }
  }, [profile, setLayer])

  return (
    <Layout className={style.container} style={{ position: 'relative' }}>
      <ScrollContainer innerRef={scrollRef as any}>
        {(imgLoader || isLoading) && (
          <div className={style.loader}>
            <Loader />
          </div>
        )}
        <div className={style.scrollContainer} onMouseMove={mouseMoveHandler}>
          <div className={style.imgContainer} ref={imgContainerRef} style={{ transform: `scale(${mapScale})` }}>
            <canvas ref={canvasObjectsRef} className={style.canvas} width={canvasSize.w} height={canvasSize.h} />
            <canvas ref={canvasLayerRef} className={style.canvas} width={canvasSize.w} height={canvasSize.h} />
            <img
              onLoad={() => setImgLoader(false)}
              ref={imgRef}
              src={map}
              alt='map'
              className={style.image}
              useMap='#map'
              onClick={mouseClickHandler}
            />
            <map name='map'>
              {areas.length > 0 &&
                areas.map((area) => {
                  return (
                    <area
                      className={style.area}
                      key={area.id}
                      shape='rect'
                      coords={area.coords}
                      onClick={() => {
                        clickHandler(area)
                      }}
                      onMouseEnter={() => mouseEnterHandler(area)}
                      onMouseLeave={mouseLeaveHandler}
                    />
                  )
                })}
            </map>
          </div>
        </div>
      </ScrollContainer>
      <div className={style.layerChooserWrapper}>
        <LayerChooser />
      </div>
      <div className={style.zoomBoxWrapper}>
        <ZoomBox mapScale={mapScale} setMapScale={setMapScale} />
      </div>
      {isTooltipVisible && (
        <Tooltip spareDirection='downStartLeft' size='s' position={position} isInteractive={false}>
          <Text size='xs'>{tooltipText}</Text>
        </Tooltip>
      )}
    </Layout>
  )
}

export default Map
