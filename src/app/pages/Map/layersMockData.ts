export type ClickableAreaType = {
  id: number
  title: string
  coords: string
  sensors: {
    id: number
    x: number
    y: number
    constructId?: number
    rotate?: number
  }[]
  thermostableSensor?: {
    id: number
    x: number
    y: number
  }
}

export type LayerType = {
  type: 'thermometricWells' | 'warpMarkers'
  areas: ClickableAreaType[]
}

export const thermometricWellsLayer: LayerType = {
  type: 'thermometricWells',
  areas: [
    {
      id: 1,
      title: 'Общежитие',
      coords: '1847,870,1956,1276',
      sensors: [{ id: 19, x: 1913, y: 1176, constructId: 1 }],
    },
    {
      id: 2,
      title: 'Столовая',
      coords: '1474,762,1789,858',
      sensors: [
        {
          id: 16,
          x: 1701,
          y: 752,
          constructId: 2,
        },
      ],
    },
    {
      id: 4,
      title: 'Банно-прачечный комплекс',
      coords: '1842,585,1964,776',
      sensors: [{ id: 14, x: 1929, y: 647, constructId: 4 }],
    },
    {
      id: 6,
      title: 'Склад продовольственных товаров',
      coords: '1460,471,1582,595',
      sensors: [
        { id: 5, x: 1460, y: 465, constructId: 6 },
        { id: 6, x: 1585, y: 465, constructId: 6 },
        { id: 7, x: 1619, y: 465, constructId: 6 },
        { id: 8, x: 1741, y: 468, constructId: 6 },
      ],
      thermostableSensor: { id: 97, x: 1565, y: 490 },
    },
    // {
    //   id: 6,
    //   title: 'Точка',
    //   coords: '1280,675,1318,708',
    //   sensors: [{ id: 14, x: 1929, y: 647 }],
    // },
    {
      id: 8,
      title: 'Овощехранилище',
      coords: '1617,471,1739,598',
      sensors: [
        { id: 10, x: 1460, y: 595, constructId: 8 },
        { id: 11, x: 1585, y: 598, constructId: 8 },
        { id: 12, x: 1619, y: 597, constructId: 8 },
        { id: 13, x: 1741, y: 597, constructId: 8 },
      ],
      thermostableSensor: { id: 98, x: 1725, y: 490 },
    },
    // {
    //   id: 9,
    //   title: 'Точка',
    //   coords: '1686,732,1717,761',
    //   sensors: [{ id: 17, x: 1630, y: 809 }],
    // },
    // {
    //   id: 10,
    //   title: 'Точка',
    //   coords: '1805,846,1840,885',
    //   sensors: [{ id: 18, x: 1823, y: 865 }],
    // },
    {
      id: 12,
      title: 'Установка подготовки и подачи питьевой воды',
      coords: '1595,239,1691,289',
      sensors: [
        { id: 1, x: 1600, y: 235, constructId: 12 },
        { id: 2, x: 1686, y: 235, constructId: 12 },
      ],
    },
    {
      id: 13,
      title: 'Подстанция водонапорная',
      coords: '1823,292,1725,263',
      sensors: [
        { id: 3, x: 1730, y: 260, constructId: 13 },
        { id: 4, x: 1825, y: 295, constructId: 13 },
      ],
    },
    // {
    //   id: 99,
    //   title: 'Точка',
    //   coords: '1585,475,1618,500',
    //   sensors: [
    //     {
    //       id: 9,
    //       x: 1318,
    //       y: 708,
    //       disabled: {
    //         from: '2021-08-01T00:00:00.000Z',
    //       },
    //     },
    //   ],
    // },
  ],
}

export const warpMarkers: LayerType = {
  type: 'warpMarkers',
  areas: [
    // {
    //   id: 4,
    //   title: 'Что-то',
    //   coords: '1291,704,1309,739',
    //   sensors: [
    //     { id: 1, x: 1285, y: 700, rotate: 45 },
    //     { id: 2, x: 1314, y: 745, rotate: 225 },
    //   ],
    // },
    // {
    //   id: 5,
    //   title: 'Емкость для бытовых стоков',
    //   coords: '1740,733,1775,752',
    //   sensors: [
    //     { id: 1, x: 1780, y: 725, rotate: 135 },
    //     { id: 2, x: 1736, y: 758, rotate: 315 },
    //   ],
    // },
    {
      id: 9,
      title: 'Мачта прожекторная 1',
      coords: '1302,967,1320,993',
      sensors: [
        { id: 1, x: 1295, y: 967 },
        { id: 2, x: 1325, y: 967, rotate: 180 },
        { id: 3, x: 1325, y: 993, rotate: 180 },
        { id: 4, x: 1295, y: 993 },
      ],
    },
    {
      id: 10,
      title: 'Мачта прожекторная 2',
      coords: '1790,1292,1807,1323',
      sensors: [
        { id: 1, x: 1783, y: 1293 },
        { id: 2, x: 1813, y: 1293, rotate: 180 },
        { id: 3, x: 1813, y: 1322, rotate: 180 },
        { id: 4, x: 1783, y: 1322 },
      ],
    },
    {
      id: 11,
      title: 'Емкость дренажная',
      coords: '1338,598,1314,630',
      sensors: [
        { id: 1, x: 1308, y: 602 },
        { id: 2, x: 1343, y: 602, rotate: 180 },
        { id: 3, x: 1343, y: 630, rotate: 180 },
        { id: 4, x: 1308, y: 630 },
      ],
    },
    {
      id: 12,
      title: 'Установка подготовки и подачи питьевой воды',
      coords: '1595,239,1691,289',
      sensors: [
        { id: 1, x: 1589, y: 238 },
        { id: 2, x: 1695, y: 238, rotate: 180 },
        { id: 3, x: 1695, y: 288, rotate: 180 },
        { id: 4, x: 1589, y: 288 },
      ],
    },
    {
      id: 13,
      title: 'Подстанция водонапорная',
      coords: '1823,292,1725,263',
      sensors: [
        { id: 1, x: 1719, y: 265 },
        { id: 2, x: 1833, y: 265, rotate: 180 },
        { id: 3, x: 1833, y: 292, rotate: 180 },
        { id: 4, x: 1719, y: 292 },
      ],
    },
  ],
}
