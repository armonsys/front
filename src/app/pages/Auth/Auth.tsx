import { Card } from '@consta/uikit/Card'
import { Layout } from '@consta/uikit/Layout'
import styles from './Auth.module.scss'
import { SignIn } from './SignIn'
import { useNavigate } from 'react-router-dom'

import { useAuth } from '@app/pages/Auth/store/store'
import { useEffect } from 'react'

const Auth = () => {
  const navigate = useNavigate()
  const profile = useAuth((state) => state.profile)

  useEffect(() => {
    if (profile) {
      navigate('/')
    }
  }, [profile, navigate])

  return (
    <Layout direction='column' className={styles.wrapper}>
      <Layout className={styles.mainBlockWrapper} direction='column'>
        <Layout className={styles.authWrapper} direction='column'>
          <Card className={styles.authCard}>
            <SignIn />
          </Card>
        </Layout>
      </Layout>
    </Layout>
  )
}

export default Auth
