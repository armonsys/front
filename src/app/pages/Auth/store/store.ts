import { createWithEqualityFn } from 'zustand/traditional'
import { GetProfileResponseType } from '../api/api'
import { shallow } from 'zustand/shallow'

interface AuthState {
  profile: GetProfileResponseType | null
  setProfile: (profile: GetProfileResponseType) => void
}
export const useAuth = createWithEqualityFn<AuthState>(
  (set) => ({
    profile: null,
    setProfile: (profile: GetProfileResponseType) => set({ profile }),
  }),
  shallow,
)
