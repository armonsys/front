import { apiClient } from '@clients/apiClient'

export type TokenResponseType = {
  access: string
  refresh: string
}

export type TokenRefreshResponseType = {
  token: string
}

export type ProfilePermissions = {
  can_view_background: boolean
  can_view_deformation: boolean
  can_view_downhole: boolean
  can_view_journal: boolean
  can_view_map: boolean
}

export type GetProfileResponseType = {
  email: string
  id: number
  is_active: boolean
  registered_at: string
  username: string
} & ProfilePermissions

export const login = async (data: { email: string; password: string }) => {
  return (await apiClient.post(`login`, data).json()) as TokenResponseType
}

export const logout = async (data: { token: string }) => {
  return (await apiClient.post(`logout`, data).json()) as null
}

export const refreshAccessToken = async (data: { token: string }) => {
  return (await apiClient.post(`update_token`, data).json()) as TokenRefreshResponseType
}

export const getProfile = async () => {
  return (await apiClient.get(`profile`).json()) as GetProfileResponseType
}
