import { apiClient } from '@clients/apiClient'
import { Layout } from '@consta/uikit/Layout'
import { Loader } from '@consta/uikit/Loader'
import { Text } from '@consta/uikit/Text'
import { TextField } from '@consta/uikit/TextField'
import { Button } from '@consta/uikit/__internal__/src/components/Button/Button'
import { validateEmail } from '@lib/helpers/validateEmail'
import { useLocalStorage } from '@lib/hooks/useLocalStorage'
import { FC, useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router'
import { toast } from 'react-toastify'

import { getProfile, login } from '../api/api'
import { useAuth } from '../store/store'
import styles from './SignIn.module.scss'

interface ISignUpProps {}

export const SignIn: FC<ISignUpProps> = () => {
  const navigate = useNavigate()
  const location = useLocation()

  const [, setJwtRefresh] = useLocalStorage<string, null>('JWTRefresh', null)
  const setProfile = useAuth((state) => state.setProfile)

  const [password, setPassword] = useState<string | null>('')
  const [isPasswordValid, setIsPasswordValid] = useState(true)
  const [isPasswordDirty, setIsPasswordDirty] = useState(false)

  const [email, setEmail] = useState<string | null>('')
  const [isEmailValid, setIsEmailValid] = useState(false)
  const [isEmailDirty, setIsEmailDirty] = useState(false)

  const [loader, setLoader] = useState(false)

  const signInHandler = async () => {
    if (!email || !password) return
    try {
      setLoader(true)
      const dataToSend = {
        email: email.trim(),
        password: password.trim(),
      }
      const resp = await login(dataToSend)

      setJwtRefresh(resp.refresh)
      apiClient.setUserToken(resp.access)

      const userProfileResp = await getProfile()
      setProfile(userProfileResp)
      if (userProfileResp) {
        if (location.state) {
          navigate(location.state.from.pathname)
        } else {
          navigate('/')
        }
      } else {
        throw new Error('Что-то пошло не так, попробуйте перезагрузить страницу')
      }
    } catch (error: any) {
      let errMessage = 'Что-то пошло не так, попробуйте перезагрузить страницу'
      if (error.response.status === 401) {
        errMessage = 'Неверный логин или пароль'
      }
      toast.error(errMessage)
      setLoader(false)
    }
  }

  useEffect(() => {
    if (password && password.trim()) {
      setIsPasswordValid(true)
    } else {
      setIsPasswordValid(false)
    }
  }, [password, isPasswordDirty])

  useEffect(() => {
    if (!email) return
    if (validateEmail(email)) {
      setIsEmailValid(true)
    } else {
      setIsEmailValid(false)
    }
  }, [email, isEmailDirty])

  return (
    <Layout direction='column' className={styles.wrapper}>
      <Text align='center' size='2xl' view='primary' className={styles.title}>
        Вход в систему
      </Text>
      <form
        onSubmit={(e) => {
          e.preventDefault()
          signInHandler()
        }}
      >
        <TextField
          onBlur={() => setIsEmailDirty(true)}
          className={styles.textField}
          width='full'
          size='l'
          label='Электронная почта'
          type='email'
          value={email}
          onChange={({ value }) => {
            setEmail(value)
          }}
          status={isEmailDirty && !isEmailValid ? 'alert' : undefined}
          caption={
            isEmailDirty && !isEmailValid ? 'Неверный адрес электронной почты' : 'Введите адрес электронной почты'
          }
          required
        />
        <div className={styles.pswdWrapper}>
          <TextField
            onBlur={() => setIsPasswordDirty(true)}
            className={styles.textField}
            width='full'
            size='l'
            label='Пароль'
            type='password'
            value={password}
            onChange={({ value }) => {
              setPassword(value)
            }}
            status={isPasswordDirty && !isPasswordValid ? 'alert' : undefined}
            caption={isPasswordDirty && !isPasswordValid ? 'Поле не может быть пустым' : 'Введите пароль'}
            required
          />
        </div>
        <Button
          // onClick={signInHandler}
          size='l'
          className={styles.btn}
          label='Войти'
          type='submit'
          disabled={!isPasswordValid || !isEmailValid}
        />
      </form>
      {loader && (
        <div className={styles.loaderContainer}>
          <Loader />
        </div>
      )}
    </Layout>
  )
}
