import { RequireAuth } from '@app/router/RequireAuth'
import { FC, lazy } from 'react'
import { Route, Routes } from 'react-router-dom'
import { Main } from './pages/Main/Main'

const KeyMonitoring = lazy(() => import('./pages/KeyMonitoring/KeyMonitoring'))
const GeoMonitoring = lazy(() => import('./pages/GeoMonitoring/GeoMonitoring'))
const BackgroundMonitoring = lazy(() => import('./pages/BackgroundMonitoring/BackgroundMonitoring'))

interface IDashboardProps {}

const Dashboard: FC<IDashboardProps> = () => {
  return (
    <>
      <Routes>
        <Route
          path='/'
          element={
            <RequireAuth>
              <Main />
            </RequireAuth>
          }
        />
        <Route
          path='/key_monitoring'
          element={
            <RequireAuth componentPerm='can_view_downhole'>
              <KeyMonitoring />
            </RequireAuth>
          }
        />
        <Route
          path='/geo_monitoring'
          element={
            <RequireAuth componentPerm='can_view_deformation'>
              <GeoMonitoring />
            </RequireAuth>
          }
        />
        <Route
          path='/background_monitoring'
          element={
            <RequireAuth componentPerm='can_view_background'>
              <BackgroundMonitoring />
            </RequireAuth>
          }
        />
      </Routes>
    </>
  )
}

export default Dashboard
