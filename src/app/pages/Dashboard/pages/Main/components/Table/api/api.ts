import { apiClient } from '@clients/apiClient'

export interface AreaType {
  id: number
  name: string
}

export interface WorkerType {
  id: number
  username: string
}

export interface JournalFetchDataType {
  id: number
  review_filename: {
    name: string
    url: string
  }
  status: 'Новое' | 'Выполнено' | 'Просмотрено'
  type: string
  created_at: string
  predicted_date: string | null
  recommendations: string
  done_at: string | null
  comments: string | null
  area: AreaType
  construct: AreaType
  worker: WorkerType | null
}

export type FetchJournalResponseType = JournalFetchDataType[]

export const fetchJournal = async () => {
  return (await apiClient.get('journal/1/').json()) as FetchJournalResponseType
}

export const patchIncident = async (data: JournalFetchDataType) => {
  return (await apiClient.patch(`journal/incident/${data.id}/`, data).json()) as FetchJournalResponseType
}
