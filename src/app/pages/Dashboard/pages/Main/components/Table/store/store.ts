import { createWithEqualityFn } from 'zustand/traditional'
import { JournalFetchDataType } from '../api/api'
import { shallow } from 'zustand/shallow'

export interface JournalDataType extends Omit<JournalFetchDataType, 'id'> {
  id: string
}

interface MainTableState {
  rows: JournalDataType[]
  setRows: (rows: JournalDataType[]) => void
}

export const useMainTable = createWithEqualityFn<MainTableState>(
  (set) => ({
    rows: [],
    setRows: (rows) => set(() => ({ rows })),
  }),
  shallow,
)
