import { useEventModal } from '@app/components/EventModal/store/store'
import { IconBento } from '@consta/icons/IconBento'
import { Table as ConstaTable, SortByProps, TableColumn, TableFilters } from '@consta/uikit/Table'
import { FC, useMemo, useState } from 'react'
import style from './Table.module.scss'
import { JournalDataType, useMainTable } from './store/store'

const columns: TableColumn<JournalDataType>[] = [
  {
    title: '',
    accessor: 'id',
    width: 50,
    renderCell: () => (
      <div className={style.iconWrapper}>
        <IconBento />
      </div>
    ),
  },
  {
    title: 'Статус',
    accessor: 'status',
    sortable: true,
    width: 140,
    renderCell: ({ status }) => (
      <div className={style.isWatchedCellContainer}>
        <span data-status={status} className={style.isWatchedDot} />
        <p>{status}</p>
      </div>
    ),
  },
  {
    title: 'Тип события',
    accessor: 'type',
    sortable: true,
    align: 'left',
  },
  {
    title: 'Дата регистрации события',
    accessor: 'created_at',
    sortable: true,
    width: 190,
    align: 'center',
    // eslint-disable-next-line camelcase
    renderCell: ({ created_at }) => (
      <div className={style.isWatchedCellContainer}>{new Date(created_at).toLocaleDateString('ru-RU')}</div>
    ),
  },
  {
    title: 'Прогнозная дата события',
    accessor: 'predicted_date',
    sortable: true,
    width: 190,
    align: 'center',
    // eslint-disable-next-line camelcase
    renderCell: ({ predicted_date }) =>
      // eslint-disable-next-line camelcase
      predicted_date ? (
        <div className={style.isWatchedCellContainer}>{new Date(predicted_date).toLocaleDateString('ru-RU')}</div>
      ) : (
        ''
      ),
  },
  {
    title: 'Рекомендация',
    accessor: 'recommendations',
    align: 'left',
  },
  {
    title: 'Дата выполнения',
    accessor: 'done_at',
    sortable: true,
    width: 190,
    align: 'center',
    // eslint-disable-next-line camelcase
    renderCell: ({ done_at }) =>
      // eslint-disable-next-line camelcase
      done_at ? (
        <div className={style.isWatchedCellContainer}>{new Date(done_at).toLocaleDateString('ru-RU')}</div>
      ) : (
        ''
      ),
  },
  {
    title: 'ФИО исполнителя',
    accessor: 'worker',
    sortable: true,

    renderCell: ({ worker }) => (worker ? <div className={style.isWatchedCellContainer}>{worker.username}</div> : ''),
  },
]

const statusFilters: TableFilters<JournalDataType> = [
  {
    id: 'Выполнено',
    name: 'Выполнено',
    filterer: (value) => value === 'Выполнено',
    field: 'status',
  },
  {
    id: 'Просмотрено',
    name: 'Просмотрено',
    filterer: (value) => value === 'Просмотрено',
    field: 'status',
  },
  {
    id: 'Новое',
    name: 'Новое',
    filterer: (value) => value === 'Новое',
    field: 'status',
  },
]

interface ITableProps {}

export const Table: FC<ITableProps> = () => {
  const setIsModalOpen = useEventModal((state) => state.setIsOpen)
  const setModalContent = useEventModal((state) => state.setModalContent)
  const data = useMainTable((state) => state.rows)

  const [sortSetting, setSortSetting] = useState<SortByProps<JournalDataType> | null>(null)

  const rows = useMemo(
    () =>
      data.sort((a, b) => {
        if (!sortSetting?.sortingBy) return 0
        if (
          sortSetting?.sortingBy === 'predicted_date' ||
          sortSetting?.sortingBy === 'created_at' ||
          sortSetting?.sortingBy === 'done_at'
        ) {
          const firstTimestamp = a?.[sortSetting?.sortingBy]
            ? new Date(a?.[sortSetting?.sortingBy] as string).getTime()
            : 0
          const secondTimestamp = b?.[sortSetting?.sortingBy]
            ? new Date(b?.[sortSetting?.sortingBy] as string).getTime()
            : 0
          const [firstDate, secondDate] =
            sortSetting.sortOrder === 'asc' ? [firstTimestamp, secondTimestamp] : [secondTimestamp, firstTimestamp]
          return firstDate - secondDate
        } else {
          const firstString =
            typeof a?.[sortSetting?.sortingBy] === 'string' ? (a?.[sortSetting?.sortingBy] as string) : ''
          const secondString =
            typeof b?.[sortSetting?.sortingBy] === 'string' ? (b?.[sortSetting?.sortingBy] as string) : ''
          const [first, second] =
            sortSetting.sortOrder === 'asc' ? [firstString, secondString] : [secondString, firstString]
          return first.localeCompare(second)
        }
      }),
    [data, sortSetting],
  )

  return (
    <div className={style.container}>
      <ConstaTable
        zebraStriped='even'
        borderBetweenColumns
        borderBetweenRows
        stickyHeader
        size='m'
        onCellClick={({ columnIdx, rowId }) => {
          if (columnIdx !== 0) return
          setIsModalOpen(true)
          const rowData = rows.find((row) => row.id === rowId)
          if (!rowData) return
          setModalContent(rowData)
        }}
        stickyColumns={2}
        columns={columns}
        rows={rows}
        onSortBy={setSortSetting}
        filters={[...statusFilters]}
        isResizable
      />
    </div>
  )
}
