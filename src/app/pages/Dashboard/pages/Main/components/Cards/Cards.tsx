import { FC } from 'react'
import style from './Cards.module.scss'
import { Layout } from '@consta/uikit/Layout'
import { Card } from '@consta/uikit/Card'
import { Text } from '@consta/uikit/Text'
import { ReactComponent as Icon } from '@assets/dashboard_icon.svg'
import { Link } from 'react-router-dom'
import { useAuth } from '@app/pages/Auth/store/store'

import { ProfilePermissions } from '@app/pages/Auth/api/api'

type CardType = {
  id: number
  title: string
  navigate: string
  access: keyof ProfilePermissions
}

const cards: CardType[] = [
  { id: 1, title: 'Ключевые показатели мониторинга', navigate: '/key_monitoring', access: 'can_view_downhole' },
  {
    id: 2,
    title: 'Геотехнический мониторинг и прогноз объектов КС',
    navigate: '/geo_monitoring',
    access: 'can_view_deformation',
  },
  { id: 3, title: 'Фоновый мониторинг', navigate: '/background_monitoring', access: 'can_view_background' },
]

interface ICardsProps {}

export const Cards: FC<ICardsProps> = () => {
  const profile = useAuth((state) => state.profile)

  return (
    <div className={style.container}>
      <Layout className={style.cardsWrapper}>
        {cards.map((card) => {
          if (profile && !profile[card.access]) return null
          return (
            <Card className={style.card} key={card.id}>
              <Link to={card.navigate} className={style.cardLink}>
                <Layout direction='column' className={style.cardWrapper}>
                  <Icon className={style.icon} />
                  <Text size='2xl' weight='bold' style={{ color: 'currentColor' }}>
                    {card.title}
                  </Text>
                </Layout>
              </Link>
            </Card>
          )
        })}
      </Layout>
    </div>
  )
}
