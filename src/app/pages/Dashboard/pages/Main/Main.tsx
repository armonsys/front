import { Layout } from '@consta/uikit/Layout'
import { FC, useLayoutEffect } from 'react'
import style from './Main.module.scss'
import { Cards } from './components/Cards/Cards'
import { Table } from './components/Table/Table'
import { useAuth } from '@app/pages/Auth/store/store'
import { useQuery } from '@tanstack/react-query'
import { fetchJournal } from './components/Table/api/api'
import { useMainTable } from './components/Table/store/store'
import { Loader } from '@consta/uikit/Loader'

interface IMainProps {}

export const Main: FC<IMainProps> = () => {
  const profile = useAuth((state) => state.profile)
  const setRows = useMainTable((state) => state.setRows)
  const { data, isLoading } = useQuery({
    queryKey: ['journal'],
    queryFn: fetchJournal,
    enabled: profile?.can_view_journal,
  })

  useLayoutEffect(() => {
    if (!data) return
    setRows(data.map((item) => ({ ...item, id: item.id.toString() })))
  }, [data, setRows])

  return (
    <Layout className={style.container} direction='column'>
      {isLoading ? (
        <div className={style.loaderContainer}>
          <Loader />
        </div>
      ) : (
        <>
          {(profile?.can_view_background || profile?.can_view_deformation || profile?.can_view_downhole) && <Cards />}
          {profile?.can_view_journal && <Table />}
        </>
      )}
    </Layout>
  )
}
