import { FC, useMemo } from 'react'
import style from './GroundTemperatureChart.module.scss'
import { Line } from '@consta/charts/Line'
import { useKeyMonitoring } from '../../store/store'

interface IGroundTemperatureChartProps {}

export const GroundTemperatureChart: FC<IGroundTemperatureChartProps> = () => {
  const groundTemperatureChartData = useKeyMonitoring((state) => state.groundTemperatureChartData)

  const minMax = useMemo(() => {
    return Math.max(...groundTemperatureChartData.map((data) => Math.abs(data.value)))
  }, [groundTemperatureChartData])

  return (
    <div className={style.container}>
      <Line
        data={groundTemperatureChartData}
        xField='depth'
        yField='value'
        seriesField='date'
        lineStyle={{
          lineWidth: 2,
        }}
        xAxis={{
          position: 'top',
          title: {
            text: 'Глубина',
            style: {
              fontSize: 16,
              fontWeight: 500,
              fontStyle: 'italic',
            },
          },
          grid: {
            line: {
              style: {
                stroke: '#9c9c9c',
                lineWidth: 1,
                lineDash: [4, 4],
              },
            },
          },
          tickLine: {
            style: {
              stroke: '#9c9c9c',
              lineWidth: 1,
            },
          },
          line: {
            style: {
              stroke: '#9c9c9c',
              lineWidth: 1,
              lineDash: [4, 4],
            },
          },
        }}
        yAxis={{
          max: minMax,
          base: 0,
          min: -minMax,
          tickInterval: 1,
          line: {
            style: {
              stroke: '#9c9c9c',
              lineWidth: 1,
              lineDash: [4, 4],
            },
          },
          grid: {
            line: {
              style: {
                stroke: '#9c9c9c',
                lineWidth: 1,
                lineDash: [4, 4],
              },
            },
          },
          tickLine: {
            style: {
              stroke: '#9c9c9c',
              lineWidth: 1,
            },
          },
          title: {
            text: 'Температура грунта t \u2103',
            style: {
              fontSize: 16,
              fontWeight: 500,
              fontStyle: 'italic',
            },
          },
        }}
        tooltip={{
          title: (depth) => {
            return `Глубина: ${depth} м`
          },
          showTitle: true,
          formatter: (datum) => {
            return { name: `${datum.date}`, value: `${datum.value} t \u2103` }
          },
        }}
        legend={{
          position: 'bottom',
          maxRow: 2,
        }}
      />
    </div>
  )
}
