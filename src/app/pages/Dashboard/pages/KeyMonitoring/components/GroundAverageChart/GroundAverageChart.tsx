import { FC } from 'react'
import style from './GroundAverageChart.module.scss'
import { Scatter } from '@consta/charts/Scatter'

import { useKeyMonitoring } from '../../store/store'

import { Layout } from '@consta/uikit/Layout'
import { Text } from '@consta/uikit/Text'

interface IGroundAverageChartProps {}

export const GroundAverageChart: FC<IGroundAverageChartProps> = () => {
  const groundAverageChartDataPerYear = useKeyMonitoring((state) => state.groundAverageChartData)
  const average = (
    groundAverageChartDataPerYear.reduce((acc, curr) => acc + curr.value, 0) / groundAverageChartDataPerYear.length
  ).toFixed(2)

  return (
    <Layout className={style.container} direction='column'>
      <Scatter
        data={groundAverageChartDataPerYear}
        xField='date'
        yField='value'
        shape='triangle'
        size={6}
        annotations={[
          {
            type: 'line',
            start: ['min', average],
            end: ['max', average],
            style: { stroke: '#666666', lineWidth: 2, lineDash: [4, 4] },
          },
        ]}
        label={{
          formatter: (datum) => {
            return datum.value.toFixed(2)
          },
          style: {
            fontSize: 12,
            fontWeight: 500,
          },
        }}
        pointStyle={{
          fillOpacity: 1,
        }}
        xAxis={{
          tickInterval: 1,
          position: 'top',
          title: {
            text: 'Средняя температура несущего слоя',
            style: {
              fontSize: 16,
              fontWeight: 500,
              fontStyle: 'italic',
            },
          },
          tickLine: {
            style: {
              stroke: '#9c9c9c',
              lineWidth: 1,
            },
          },
          line: {
            style: {
              stroke: '#9c9c9c',
              lineWidth: 1,
              lineDash: [4, 4],
            },
          },
        }}
        yAxis={{
          max: 2,
          base: 0,
          tickInterval: 0.5,
          line: {
            style: {
              stroke: '#9c9c9c',
              lineWidth: 1,
              lineDash: [4, 4],
            },
          },
          grid: {
            line: {
              style: {
                stroke: '#9c9c9c',
                lineWidth: 1,
                lineDash: [4, 4],
              },
            },
          },
          tickLine: {
            style: {
              stroke: '#9c9c9c',
              lineWidth: 1,
            },
          },
          title: {
            text: 'Температура t \u2103',
            style: {
              fontSize: 16,
              fontWeight: 500,
              fontStyle: 'italic',
            },
          },
        }}
        tooltip={{
          showTitle: true,
          formatter: (datum) => {
            return { name: 'Cредняя температура', value: `${datum.value.toFixed(2)} t \u2103` }
          },
        }}
      />
      <Layout className={style.legend}>
        <Layout direction='column'>
          <div className={style.legendItemContainer}>
            <span className={style.dashedLine}></span>
            <Text size='s'>{`Линейная (Средняя t \u2103)`}</Text>
          </div>
        </Layout>
      </Layout>
    </Layout>
  )
}
