import { memo, FC, useEffect, useState } from 'react'
import style from './KeyMonitoringTable.module.scss'
import { Table as ConstaTable, TableColumn } from '@consta/uikit/Table'
import { useKeyMonitoring } from '../../store/store'

interface IKeyMonitoringTableProps {}

const KeyMonitoringTableInner: FC<IKeyMonitoringTableProps> = () => {
  const downholeConstructData = useKeyMonitoring((state) => state.downholeConstructData)

  const currentDownHole = useKeyMonitoring((state) => state.currentDownHole)
  const [groundTemperatureChartData, setGroundTemperatureChartData] = useKeyMonitoring((state) => [
    state.groundTemperatureChartData,
    state.setGroundTemperatureChartData,
  ])

  const setGroundAverageChartData = useKeyMonitoring((state) => state.setGroundAverageChartData)

  const [columns, setColumns] = useState<TableColumn<any>[]>([])
  const [rows, setRows] = useState<any>([])

  const getCellColor = (value: number) => {
    if (value < -1) return `rgba(50, 204, 255, ${Math.abs(value * 3)}%)`
    if (value > -1) return `rgba(248, 105, 107, ${Math.abs(value * 3)}%)`
    return 'unset'
  }

  const addToChartDataHandler = (columnIdx: number) => {
    const currentSensor = downholeConstructData?.downhole_sensors.find((sensor) => sensor.id === currentDownHole?.id)
    const dateAccessor = columns[columnIdx].accessor
    if (groundTemperatureChartData.some((data: any) => data.date === dateAccessor)) {
      setGroundTemperatureChartData(groundTemperatureChartData.filter((data) => data.date !== dateAccessor))
      return
    }
    if (!currentSensor || !dateAccessor) return
    const newChartData = currentSensor.data[dateAccessor].map((data) => {
      return { ...data, depth: data.depth.toString(), date: new Date(dateAccessor).toLocaleDateString('ru-Ru') }
    })

    const newGroundTemperatureChartData = [...groundTemperatureChartData, ...newChartData].sort(
      (a, b) => Number(a.depth) - Number(b.depth),
    )
    setGroundTemperatureChartData(newGroundTemperatureChartData)
  }

  useEffect(() => {
    const currentSensor = downholeConstructData?.downhole_sensors.find((sensor) => sensor.id === currentDownHole?.id)
    if (!currentSensor) return
    const rowsArr = [] as { [key: string]: number | string }[]
    const averageRow = { id: 'average', depth: 'Средняя t \u2103' } as {
      [key: string]: number | string | { average: number }
    }
    const emptyRow = { id: 'empty' } as { [key: string]: '\u3000' | 'empty' }
    const averageChartData = [] as { date: string; value: number }[]
    Object.keys(currentSensor.data).forEach((date) => {
      const average =
        currentSensor.data[date].reduce((acc, curr) => acc + curr.value, 0) / currentSensor.data[date].length
      averageChartData.push({ date: new Date(date).toLocaleDateString('ru-Ru'), value: average })
      averageRow[date] = {
        average,
      }
      emptyRow[date] = '\u3000'
      currentSensor.data[date].forEach((data, idx) => {
        rowsArr[data.depth - 1] = {
          ...rowsArr[data.depth - 1],
          id: idx,
          depth: data.depth,
          [date]: data.value,
        }
      })
    })
    setGroundAverageChartData(averageChartData)
    setRows([...rowsArr, emptyRow, averageRow])
    setColumns([
      {
        title: 'Глубина, м',
        accessor: 'depth',
        width: 100,
        align: 'center',
        renderCell: ({ depth }) => {
          return <div className={style.cell}>{depth}</div>
        },
      },
      ...Object.keys(currentSensor.data).map((date) => {
        return {
          title: new Date(date).toLocaleDateString('ru-RU'),
          accessor: date,
          width: 90,
          renderCell: (data: { [key: string]: number | any }) => {
            if (typeof data[date] === 'object' && 'average' in data[date]) {
              return (
                <div className={style.cell} style={{ background: `${getCellColor(data[date].average)}` }}>
                  {data[date].average.toFixed(2)}
                </div>
              )
            }
            return (
              <div className={style.cell} style={{ background: `${getCellColor(data[date])}` }}>
                {data[date]}
              </div>
            )
          },
        }
      }),
    ])
    const firstDate = Object.keys(currentSensor.data)[0]
    const lastDate = Object.keys(currentSensor.data)[Object.keys(currentSensor.data).length - 1]
    const firstDateData = currentSensor.data[firstDate].map((data) => {
      return { ...data, depth: data.depth.toString(), date: new Date(firstDate).toLocaleDateString('ru-Ru') }
    })
    const lastDateData = currentSensor.data[lastDate].map((data) => {
      return { ...data, depth: data.depth.toString(), date: new Date(lastDate).toLocaleDateString('ru-Ru') }
    })
    setGroundTemperatureChartData([...firstDateData, ...lastDateData].sort((a, b) => Number(a.depth) - Number(b.depth)))
  }, [downholeConstructData, currentDownHole, setGroundTemperatureChartData, setGroundAverageChartData])

  return (
    <div className={style.container}>
      {rows.length > 2 && columns.length > 0 && (
        <ConstaTable
          borderBetweenColumns
          borderBetweenRows
          stickyHeader
          size='m'
          onCellClick={({ columnIdx }) => addToChartDataHandler(columnIdx)}
          stickyColumns={1}
          columns={columns}
          rows={rows}
        />
      )}
    </div>
  )
}

export const KeyMonitoringTable = memo(KeyMonitoringTableInner)
