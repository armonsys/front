import { FC, useEffect, useState } from 'react'
import style from './KeyMonitoring.module.scss'
import { Layout } from '@consta/uikit/Layout'
import { ObjectsSelect } from '@app/components/ObjectsSelect/ObjectsSelect'
import { ThermoWellSelect } from '../../../../components/ThermoWellSelect/ThermoWellSelect'
import { Text } from '@consta/uikit/Text'
import { useQuery } from '@tanstack/react-query'
import { useObjectsSelect } from '@app/components/ObjectsSelect/store/store'

import { Loader } from '@consta/uikit/Loader/index'
import { fetchDownholeSensorsByConstructId } from './api/api'
import { useKeyMonitoring } from './store/store'
import { thermometricWellsObjects } from '../GeoMonitoring/objectsMockData'
import { useSearchParams } from 'react-router-dom'
import { KeyMonitoringTable } from './components/KeyMonitoringTable/KeyMonitoringTable'
import { GroundTemperatureChart } from './components/GroundTemperatureChart/GroundTemperatureChart'
import { GroundAverageChart } from './components/GroundAverageChart/GroundAverageChart'

export type Item = {
  label: string
  id: number
}

interface IKeyMonitoringProps {}

const KeyMonitoring: FC<IKeyMonitoringProps> = () => {
  const [searchParams] = useSearchParams()
  const [isFirstRender, setIsFirstRender] = useState(true)
  const [currentConstruct, setCurrentConstruct] = useObjectsSelect((state) => [
    state.chosenConstruct,
    state.setChosenConstruct,
  ])

  const setObjectsArr = useObjectsSelect((state) => state.setObjectsArr)

  const setDownholeConstructData = useKeyMonitoring((state) => state.setDownholeConstructData)

  const [currentDownHole, setCurrentDownHole] = useKeyMonitoring((state) => [
    state.currentDownHole,
    state.setCurrentDownHole,
  ])

  const { status, isFetching, data } = useQuery({
    queryKey: ['downhole:construct', currentConstruct?.id],
    queryFn: () => fetchDownholeSensorsByConstructId(currentConstruct?.id ?? null),
    enabled: !!currentConstruct?.id,
  })

  const [downholeSensorsArr, setDownholeSensorsArr] = useState<{ id: number; label: string }[]>([])
  const [currWell, setCurrWell] = useState<{ id: number; label: string } | null>(null)

  useEffect(() => {
    setObjectsArr(thermometricWellsObjects)
    setCurrentConstruct(thermometricWellsObjects[0])

    return () => {
      setObjectsArr([])
      setCurrentConstruct(null)
    }
  }, [setObjectsArr, setCurrentConstruct])

  useEffect(() => {
    const idFromSearchParams = Number(searchParams.get('id'))
    if (idFromSearchParams) {
      const constructFromSearchParams = thermometricWellsObjects.find((el) => el.id === idFromSearchParams)
      if (constructFromSearchParams) {
        setCurrentConstruct(constructFromSearchParams)
      }
    }
  }, [searchParams, setCurrentConstruct])

  useEffect(() => {
    if (!data?.downhole_sensors.length) return
    const sensorsArr = data?.downhole_sensors.map((sensor) => {
      return { id: sensor.id, label: sensor.name }
    })
    setDownholeSensorsArr(sensorsArr)
    setCurrWell(sensorsArr[0])
    setDownholeConstructData(data)
  }, [data, setDownholeConstructData])

  useEffect(() => {
    if (!currWell) return
    const downHoleSensor = data?.downhole_sensors.find((sensor) => sensor.id === currWell.id)
    if (!downHoleSensor) return
    setCurrentDownHole(downHoleSensor)
  }, [currWell, data, setCurrentDownHole])

  useEffect(() => {
    if (!data) return
    setIsFirstRender(false)
  }, [data])

  return (
    <Layout className={style.container} direction='column'>
      {status === 'loading' && isFirstRender ? (
        <div className={style.loaderContainer}>
          <Loader />
        </div>
      ) : (
        <>
          {isFetching && (
            <div className={style.fetchLoaderContainer}>
              <Loader />
            </div>
          )}
          <Layout className={style.selectWrapper}>
            <ThermoWellSelect currWell={currWell} setCurrWell={setCurrWell} items={downholeSensorsArr} />
            <ObjectsSelect />
          </Layout>
          <Layout className={style.titleWrapper}>
            <Text className={style.title} size='2xl' weight='bold'>
              {currWell?.label}
            </Text>
          </Layout>
          {currentDownHole?.data && Object.keys(currentDownHole?.data)?.length > 0 ? (
            <>
              <Layout className={style.chartsWrapper}>
                <Layout className={style.chartWrapper}>
                  <GroundAverageChart />
                </Layout>
                <Layout className={style.chartWrapper}>
                  <GroundTemperatureChart />
                </Layout>
              </Layout>
              <Layout className={style.tableWrapper}>
                <Layout className={style.table}>
                  <KeyMonitoringTable />
                </Layout>
              </Layout>
            </>
          ) : (
            <Layout className={style.emptyContainer}>
              <Text size='3xl'>Нет данных</Text>
            </Layout>
          )}
        </>
      )}
    </Layout>
  )
}

export default KeyMonitoring
