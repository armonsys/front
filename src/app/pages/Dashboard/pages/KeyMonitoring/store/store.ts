import { createWithEqualityFn } from 'zustand/traditional'
import { DownholeConstructData } from '../api/api'
import { shallow } from 'zustand/shallow'

interface KeyMonitoringState {
  downholeConstructData: DownholeConstructData | undefined
  setDownholeConstructData: (data: DownholeConstructData) => void
  currentDownHole: DownholeConstructData['downhole_sensors'][number] | null
  setCurrentDownHole: (data: DownholeConstructData['downhole_sensors'][number]) => void
  groundTemperatureChartData: { depth: string; value: number; date: string }[]
  setGroundTemperatureChartData: (data: { depth: string; value: number; date: string }[]) => void
  groundAverageChartData: { date: string; value: number }[]
  setGroundAverageChartData: (data: { date: string; value: number }[]) => void
}

export const useKeyMonitoring = createWithEqualityFn<KeyMonitoringState>(
  (set) => ({
    downholeConstructData: undefined,
    setDownholeConstructData: (data) => set(() => ({ downholeConstructData: data })),
    currentDownHole: null,
    setCurrentDownHole: (data) => set(() => ({ currentDownHole: data })),
    groundTemperatureChartData: [],
    setGroundTemperatureChartData: (data: any) => set(() => ({ groundTemperatureChartData: data })),
    groundAverageChartData: [],
    setGroundAverageChartData: (data: any) => set(() => ({ groundAverageChartData: data })),
  }),
  shallow,
)
