import { apiClient } from '@clients/apiClient'

export interface Datum {
  value: number
  depth: number
}

export interface DownholeSensor {
  id: number
  name: string
  description: null
  data: { [key: string]: Datum[] }
}

export interface DownholeConstructData {
  id: number
  name: string
  created_at: Date
  max_temp_downhole: null
  downhole_sensors: DownholeSensor[]
}

type FetchDownholeSensorsByConstructIdResponse = DownholeConstructData

export const fetchDownholeSensorsByConstructId = async (constructId: number | null) => {
  if (constructId) {
    return (await apiClient
      .get(`constructs/downhole/${constructId}/`)
      .json()) as FetchDownholeSensorsByConstructIdResponse
  }
}
