import { createWithEqualityFn } from 'zustand/traditional'
import { FetchAirTempRespType, FetchSnowRespType, FetchWindRespType } from '../api/api'
import { shallow } from 'zustand/shallow'

interface BackgroundMonitoringState {
  airTempData: FetchAirTempRespType
  setAirTempData: (data: FetchAirTempRespType) => void
  windData: FetchWindRespType
  setWindData: (data: FetchWindRespType) => void
  snowData: FetchSnowRespType
  setSnowData: (data: FetchSnowRespType) => void
}
export const useBackgroundMonitoring = createWithEqualityFn<BackgroundMonitoringState>(
  (set) => ({
    airTempData: [],
    setAirTempData: (data) => set({ airTempData: data }),
    windData: [],
    setWindData: (data) => set({ windData: data }),
    snowData: [],
    setSnowData: (data) => set({ snowData: data }),
  }),
  shallow,
)
