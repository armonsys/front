import { Card } from '@consta/uikit/Card'
import { Layout } from '@consta/uikit/Layout'
import { Text } from '@consta/uikit/Text'
import { useQuery } from '@tanstack/react-query'
import { FC, useMemo } from 'react'
import style from './BackgroundMonitoring.module.scss'
import { fetchAirTemp, fetchGroundTemp, fetchSnow, fetchWind } from './api/api'
import { BackgroundChart } from './components/BackgroundChart/BackgroundChart'
import { Loader } from '@consta/uikit/Loader'
import { BackgroundTable } from './components/BackgroundTable/BackgroundTable'

interface IBackgroundMonitoringProps {}

const BackgroundMonitoring: FC<IBackgroundMonitoringProps> = () => {
  const { data: airTempData } = useQuery({
    queryKey: ['wind'],
    queryFn: () => fetchAirTemp(),
  })
  const { data: windData } = useQuery({
    queryKey: ['airTemp'],
    queryFn: () => fetchWind(),
  })
  const { data: snowData } = useQuery({
    queryKey: ['snow'],
    queryFn: () => fetchSnow(),
  })
  const { data: groundTemp } = useQuery({
    queryKey: ['groundTemp'],
    queryFn: () => fetchGroundTemp(),
  })

  const rows = useMemo(
    () => [
      {
        id: 1,
        title: 'Температура воздуха',
        chartTitle: 'Температура \u2103',
        units: '\u2103',
        data: airTempData,
      },
      {
        id: 2,
        title: 'Скорость ветра',
        chartTitle: 'Скорость ветра, м/с',
        units: 'м/с',
        data: windData,
        cellColorReverse: true,
      },
      {
        id: 3,
        title: 'Высота снежного покрова',
        chartTitle: 'Высота снежного покрова, см',
        units: 'см',
        data: snowData,
        cellColorReverse: true,
      },
      {
        id: 4,
        title: 'Температура грунтов (вне зоны техногенного влияния)',
        chartTitle: 'Температура грунта, \u2103',
        units: '\u2103',
        data: groundTemp,
      },
    ],
    [airTempData, windData, snowData, groundTemp],
  )

  return (
    <Layout className={style.container} direction='column'>
      {!airTempData || !windData || !snowData ? (
        <div className={style.loaderContainer}>
          <Loader />
        </div>
      ) : (
        rows.map((item, idx) => (
          <Layout key={item.id} direction='column' className={style.rowWrapper}>
            <Text className={style.title} size='2xl' weight='bold'>
              {item.title}
            </Text>
            <Layout className={style.chartsWrapper}>
              {idx % 2 === 0 ? (
                <>
                  <Card className={style.chartContainer}>
                    <BackgroundChart data={item.data} chartTitle={item.chartTitle} units={item.units} />
                  </Card>
                  <Card className={style.chartContainer}>
                    <BackgroundTable data={item.data} cellColorReverse={item.cellColorReverse} />
                  </Card>
                </>
              ) : (
                <>
                  <Card className={style.chartContainer}>
                    <BackgroundTable data={item.data} cellColorReverse={item.cellColorReverse} />
                  </Card>
                  <Card className={style.chartContainer}>
                    <BackgroundChart data={item.data} chartTitle={item.chartTitle} units={item.units} />
                  </Card>
                </>
              )}
            </Layout>
          </Layout>
        ))
      )}
    </Layout>
  )
}

export default BackgroundMonitoring
