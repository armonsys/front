import { apiClient } from '@clients/apiClient'

export interface RespType {
  year: string
  month: string
  avg_1: number
}

export type FetchAirTempRespType = RespType[]
export type FetchWindRespType = RespType[]
export type FetchSnowRespType = RespType[]

export const fetchAirTemp = async () => {
  return (await apiClient.get(`air-temp/1/`).json()) as FetchAirTempRespType
}

export const fetchWind = async () => {
  return (await apiClient.get(`wind/1/`).json()) as FetchWindRespType
}

export const fetchSnow = async () => {
  return (await apiClient.get(`snow/1/`).json()) as FetchSnowRespType
}

export const fetchGroundTemp = async () => {
  return (await apiClient.get(`ground-temp/1/`).json()) as FetchSnowRespType
}
