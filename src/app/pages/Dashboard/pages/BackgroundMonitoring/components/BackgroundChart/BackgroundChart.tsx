import { FC, useMemo } from 'react'
import style from './BackgroundChart.module.scss'
import { FetchAirTempRespType, FetchSnowRespType, FetchWindRespType, RespType } from '../../api/api'
import { Line } from '@consta/charts/Line'
import { calc } from './approximationHelper'

interface IBackgroundChartProps {
  data: FetchAirTempRespType | FetchSnowRespType | FetchWindRespType | undefined
  chartTitle: string
  units: string
}

export const BackgroundChart: FC<IBackgroundChartProps> = ({ data, chartTitle, units }) => {
  const serializedData = useMemo(() => {
    const avgDataPerYear = {} as { [key: string]: RespType[] }

    data?.forEach((item) => {
      const year = item.year.split('-')[0]
      if (!avgDataPerYear[year]) {
        avgDataPerYear[year] = []
      }
      avgDataPerYear[year].push(item)
    })
    return Object.keys(avgDataPerYear).map((year) => {
      const avg =
        avgDataPerYear[year].reduce((acc, item) => {
          return acc + item.avg_1
        }, 0) / avgDataPerYear[year].length

      return {
        year,
        avg,
        title: 'Среднее за год',
      }
    })
  }, [data])

  const linearAverage = useMemo(() => {
    const trend = calc(
      serializedData.map((item) => {
        return {
          ...item,
          year: new Date(item.year).getTime(),
        }
      }),
      'year',
      'avg',
    )
    return [
      {
        avg: trend.calcY(new Date(serializedData[0].year).getTime()),
        year: serializedData[0].year,
        title: 'Линейная (среднее за год)',
      },
      {
        avg: trend.calcY(new Date(serializedData[serializedData.length - 1].year).getTime()),
        year: serializedData[serializedData.length - 1].year,
        title: 'Линейная (среднее за год)',
      },
    ]
  }, [serializedData])

  const combinedData = useMemo(() => {
    return [...serializedData, ...linearAverage]
  }, [serializedData, linearAverage])

  if (!data) {
    return null
  }

  return (
    <div className={style.container}>
      <Line
        data={combinedData}
        xField='year'
        yField='avg'
        smooth
        seriesField='title'
        xAxis={{
          position: 'top',
          grid: {
            line: {
              style: {
                stroke: '#9c9c9c',
                lineWidth: 1,
                lineDash: [4, 4],
              },
            },
          },
          tickLine: {
            style: {
              stroke: '#9c9c9c',
              lineWidth: 1,
            },
          },
          line: {
            style: {
              stroke: '#9c9c9c',
              lineWidth: 1,
              lineDash: [4, 4],
            },
          },
        }}
        yAxis={{
          line: {
            style: {
              stroke: '#9c9c9c',
              lineWidth: 1,
              lineDash: [4, 4],
            },
          },
          grid: {
            line: {
              style: {
                stroke: '#9c9c9c',
                lineWidth: 1,
                lineDash: [4, 4],
              },
            },
          },
          tickLine: {
            style: {
              stroke: '#9c9c9c',
              lineWidth: 1,
            },
          },
          title: {
            text: chartTitle,
            style: {
              fontSize: 16,
              fontWeight: 500,
              fontStyle: 'italic',
            },
          },
        }}
        tooltip={{
          showTitle: true,
          formatter: (datum) => {
            return { name: datum.title, value: `${datum.avg.toFixed(2)} ${units}` }
          },
        }}
        legend={{
          position: 'bottom',
          maxRow: 2,
          track: {
            style: {
              stroke: '#c55a11',
              lineWidth: 2,
            },
          },
          // items: [{ name: 'Среднее за год', value: 'avg', marker: { style: { lineWidth: 20, fill: '#c55a11' } } }],
        }}
      />
    </div>
  )
}
