import { FC, useEffect, useMemo, useState } from 'react'
import style from './BackgroundTable.module.scss'
import { FetchAirTempRespType, FetchSnowRespType, FetchWindRespType } from '../../api/api'
import { Table as ConstaTable, TableColumn } from '@consta/uikit/Table'

interface IBackgroundTableProps {
  data: FetchAirTempRespType | FetchSnowRespType | FetchWindRespType | undefined
  cellColorReverse?: boolean
}

export const BackgroundTable: FC<IBackgroundTableProps> = ({ data, cellColorReverse = false }) => {
  const [rows, setRows] = useState<any>([])

  const maxValue = useMemo(() => {
    if (!data) return 0
    return Math.max(...data.map((item) => item.avg_1))
  }, [data])

  const minValue = useMemo(() => {
    if (!data) return 0
    return Math.min(...data.map((item) => item.avg_1))
  }, [data])

  const getCellColor = (value: number) => {
    const threshold = 0
    if (cellColorReverse) {
      if (value > threshold) return `rgba(50, 204, 255, ${Math.abs((value / maxValue) * 100)}%)`
      if (value < -threshold) return `rgba(248, 105, 107, ${Math.abs((value / minValue) * 100)}%)`
    } else {
      if (value > -threshold) return `rgba(248, 105, 107, ${Math.abs((value / maxValue) * 100)}%)`
      if (value < threshold) return `rgba(50, 204, 255, ${Math.abs((value / minValue) * 100)}%)`
    }

    return 'unset'
  }

  const columns: TableColumn<any>[] = [
    {
      title: 'Дата',
      accessor: 'year',
      align: 'center',
      renderCell: ({ year }) => {
        if (!year) return null
        return <div className={style.cell}>{year}</div>
      },
    },
    {
      title: '1',
      accessor: '01',
      align: 'center',
      renderCell: (data) => {
        if (!data['01'] && data['01'] !== 0) return null
        return (
          <div className={style.cell} style={{ background: `${getCellColor(data['01'])}` }}>
            {data['01'].toFixed(2)}
          </div>
        )
      },
    },
    {
      title: '2',
      accessor: '02',

      align: 'center',
      renderCell: (data) => {
        if (!data['02'] && data['02'] !== 0) return null
        return (
          <div className={style.cell} style={{ background: `${getCellColor(data['02'])}` }}>
            {data['02'].toFixed(2)}
          </div>
        )
      },
    },
    {
      title: '3',
      accessor: '03',

      align: 'center',
      renderCell: (data) => {
        if (!data['03'] && data['03'] !== 0) return null
        return (
          <div className={style.cell} style={{ background: `${getCellColor(data['03'])}` }}>
            {data['03'].toFixed(2)}
          </div>
        )
      },
    },
    {
      title: '4',
      accessor: '04',
      align: 'center',
      renderCell: (data) => {
        if (!data['04'] && data['04'] !== 0) return null
        return (
          <div className={style.cell} style={{ background: `${getCellColor(data['04'])}` }}>
            {data['04'].toFixed(2)}
          </div>
        )
      },
    },
    {
      title: '5',
      accessor: '05',
      align: 'center',
      renderCell: (data) => {
        if (!data['05'] && data['05'] !== 0) return null
        return (
          <div className={style.cell} style={{ background: `${getCellColor(data['05'])}` }}>
            {data['05'].toFixed(2)}
          </div>
        )
      },
    },
    {
      title: '6',
      accessor: '06',
      align: 'center',
      renderCell: (data) => {
        if (!data['06'] && data['06'] !== 0) return null
        return (
          <div className={style.cell} style={{ background: `${getCellColor(data['06'])}` }}>
            {data['06'].toFixed(2)}
          </div>
        )
      },
    },
    {
      title: '7',
      accessor: '07',
      align: 'center',
      renderCell: (data) => {
        if (!data['07'] && data['07'] !== 0) return null
        return (
          <div className={style.cell} style={{ background: `${getCellColor(data['07'])}` }}>
            {data['07'].toFixed(2)}
          </div>
        )
      },
    },
    {
      title: '8',
      accessor: '08',
      align: 'center',
      renderCell: (data) => {
        if (!data['08'] && data['08'] !== 0) return null
        return (
          <div className={style.cell} style={{ background: `${getCellColor(data['08'])}` }}>
            {data['08'].toFixed(2)}
          </div>
        )
      },
    },
    {
      title: '9',
      accessor: '09',
      align: 'center',
      renderCell: (data) => {
        if (!data['09'] && data['09'] !== 0) return null
        return (
          <div className={style.cell} style={{ background: `${getCellColor(data['09'])}` }}>
            {data['09'].toFixed(2)}
          </div>
        )
      },
    },
    {
      title: '10',
      accessor: '10',
      align: 'center',
      renderCell: (data) => {
        if (!data['10'] && data['10'] !== 0) return null
        return (
          <div className={style.cell} style={{ background: `${getCellColor(data['10'])}` }}>
            {data['10'].toFixed(2)}
          </div>
        )
      },
    },
    {
      title: '11',
      accessor: '11',
      align: 'center',
      renderCell: (data) => {
        if (!data['11'] && data['11'] !== 0) return null
        return (
          <div className={style.cell} style={{ background: `${getCellColor(data['11'])}` }}>
            {data['11'].toFixed(2)}
          </div>
        )
      },
    },
    {
      title: '12',
      accessor: '12',
      align: 'center',
      renderCell: (data) => {
        if (!data['12'] && data['12'] !== 0) return null
        return (
          <div className={style.cell} style={{ background: `${getCellColor(data['12'])}` }}>
            {data['12'].toFixed(2)}
          </div>
        )
      },
    },
    {
      title: 'Среднее',
      accessor: 'avgPerYear',
      align: 'center',
      renderCell: ({ avgPerYear }) => {
        if (!avgPerYear && avgPerYear !== 0) return null
        return (
          <div className={style.cell} style={{ background: `${getCellColor(avgPerYear)}` }}>
            {avgPerYear.toFixed(2)}
          </div>
        )
      },
    },
  ]

  useEffect(() => {
    const avgDataPerYear = {} as { [key: string]: any }
    data?.forEach((item) => {
      const year = item.year.split('-')[0]
      if (!avgDataPerYear[year]) {
        avgDataPerYear[year] = {}
      }
      avgDataPerYear[year][item.month.split('-')[1]] = item.avg_1
      avgDataPerYear[year].year = year
      avgDataPerYear[year].id = year
    })

    const rows = Object.values(avgDataPerYear).map((dataPerYear) => {
      const monthKeys = Object.keys(dataPerYear).filter((key) => key !== 'year' && !isNaN(Number(key)))
      return {
        ...dataPerYear,
        avgPerYear:
          monthKeys.reduce((acc, key) => (!isNaN(Number(dataPerYear[key])) ? acc + Number(dataPerYear[key]) : acc), 0) /
          monthKeys.length,
      }
    })
    const sumPerMonthInAllYears = {} as any
    const minPerMonthInYears = { year: 'Мин', id: 'min' } as any
    const maxPerMonthInYears = { year: 'Макс', id: 'max' } as any

    rows.forEach((yearValue) => {
      Object.keys(yearValue).forEach((monthKey) => {
        if (monthKey === 'year' || monthKey === 'id' || isNaN(yearValue[monthKey])) return
        sumPerMonthInAllYears[monthKey] = sumPerMonthInAllYears[monthKey]
          ? Number(sumPerMonthInAllYears[monthKey]) + Number(yearValue[monthKey])
          : yearValue[monthKey]

        minPerMonthInYears[monthKey] = minPerMonthInYears[monthKey]
          ? Math.min(minPerMonthInYears[monthKey], yearValue[monthKey])
          : yearValue[monthKey]

        maxPerMonthInYears[monthKey] = maxPerMonthInYears[monthKey]
          ? Math.max(maxPerMonthInYears[monthKey], yearValue[monthKey])
          : yearValue[monthKey]
      })
    })

    const averagePerMonthInAllYears = { year: 'Среднее', id: 'avg' } as any

    Object.keys(sumPerMonthInAllYears).forEach((month) => {
      averagePerMonthInAllYears[month] = sumPerMonthInAllYears[month] / rows.filter((row) => row[month]).length
    })
    setRows([...rows, averagePerMonthInAllYears, minPerMonthInYears, maxPerMonthInYears])
  }, [data])

  if (!data) {
    return null
  }
  return (
    <div className={style.container}>
      {rows.length > 2 && columns.length > 0 && (
        <ConstaTable
          borderBetweenColumns
          borderBetweenRows
          stickyHeader
          size='m'
          stickyColumns={1}
          columns={columns}
          rows={rows}
        />
      )}
    </div>
  )
}
