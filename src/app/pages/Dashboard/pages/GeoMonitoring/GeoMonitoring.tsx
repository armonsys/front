import { ObjectsSelect } from '@app/components/ObjectsSelect/ObjectsSelect'
import dotWithArrows from '@assets/dot_with_arrows.svg'
import { Card } from '@consta/uikit/Card'
import { Layout } from '@consta/uikit/Layout'
import { Text } from '@consta/uikit/Text'
import { FC, useCallback, useEffect, useRef } from 'react'
import style from './GeoMonitoring.module.scss'

import { useObjectsSelect } from '@app/components/ObjectsSelect/store/store'
import { Loader } from '@consta/uikit/Loader/index'
import { useQuery } from '@tanstack/react-query'

import { fetchDeformationSensorsByConstructId } from './api/api'
import { ChartsContainers } from './components/ChartsContainers/ChartsContainers'
import { Cube } from './components/Cube/Cube'
import { warpMarkersObjects } from './objectsMockData'
import { useGeoMonitoring } from './store/store'
import { useSearchParams } from 'react-router-dom'

interface IGeoMonitoringProps {}

const GeoMonitoring: FC<IGeoMonitoringProps> = () => {
  const [searchParams] = useSearchParams()

  const backCanvasRef = useRef<HTMLCanvasElement>(null)
  const frontCanvasRef = useRef<HTMLCanvasElement>(null)
  const containerRefsArr = useRef<HTMLDivElement[]>([])
  const cubeSideRefsArr = useRef<HTMLDivElement[]>([])
  const containerRef = useRef<HTMLDivElement>(null)

  const [currentConstruct, setCurrentConstruct] = useObjectsSelect((state) => [
    state.chosenConstruct,
    state.setChosenConstruct,
  ])
  const setObjectsArr = useObjectsSelect((state) => state.setObjectsArr)
  const setActiveStepIndex = useGeoMonitoring((state) => state.setActiveStepIndex)
  const [deformationConstructData, setDeformationConstructData] = useGeoMonitoring((state) => [
    state.deformationConstructData,
    state.setDeformationConstructData,
  ])

  const { status, isFetching, data } = useQuery({
    queryKey: ['deformation:construct', currentConstruct?.id],
    queryFn: () => fetchDeformationSensorsByConstructId(currentConstruct?.id ?? null),
    enabled: !!currentConstruct?.id,
  })

  const getCoords = () => {
    if (!containerRef.current) return []
    const mainContainer = containerRef.current
    const mainContainerRect = mainContainer.getBoundingClientRect()
    return containerRefsArr.current.map((el, idx) => {
      const containerRect = el.getBoundingClientRect()
      const cubeSideRect = cubeSideRefsArr.current[idx].getBoundingClientRect()
      return {
        isWarn: el.dataset.isWarn === 'true',
        from: {
          x: idx === 0 || idx === 2 ? el.offsetLeft + containerRect.width - 3 : el.offsetLeft + 3,
          y: el.offsetTop + 3,
        },
        to: {
          x:
            idx === 2 || idx === 3
              ? cubeSideRect.left - mainContainer.offsetLeft + cubeSideRect.width
              : cubeSideRect.right - mainContainer.offsetLeft - cubeSideRect.width,
          // this magic numbers are for better look of lines. Because center of edge is not center of cube side
          y: idx === 2 || idx === 3 ? mainContainerRect.height / 2 + 40 : mainContainerRect.height / 2 - 30,
        },
      }
    })
  }

  const drawLines = useCallback(() => {
    const frontCanvas = frontCanvasRef.current
    const backCanvas = backCanvasRef.current
    const containerSize = containerRef.current?.getBoundingClientRect()
    if (!backCanvas || !frontCanvas || !containerSize) return
    frontCanvas.width = backCanvas.width = containerSize.width
    frontCanvas.height = backCanvas.height = containerSize.height
    const frontCtx = frontCanvas.getContext('2d')
    const backCtx = backCanvas.getContext('2d')
    if (!frontCtx || !backCtx) return

    frontCtx.clearRect(0, 0, frontCanvas.width, frontCanvas.height)
    backCtx.clearRect(0, 0, backCanvas.width, backCanvas.height)

    const linesArr = getCoords()
    const img = new Image()
    img.src = dotWithArrows
    img.onload = () => {
      linesArr.forEach((coords, idx) => {
        const currCtx = idx > 1 ? frontCtx : backCtx
        currCtx.strokeStyle = '#000000'
        currCtx.beginPath()
        currCtx?.moveTo(coords.from.x, coords.from.y)
        currCtx?.lineTo(coords.to.x, coords.to.y)
        currCtx.lineWidth = 0.5
        currCtx.stroke()
        currCtx?.drawImage(img, coords.to.x - img.width / 2, coords.to.y - img.height / 2)
      })
    }
  }, [])

  useEffect(() => {
    if (status === 'loading') return
    if (!data) return
    setDeformationConstructData(data)
    setActiveStepIndex(data.deformation_sensors[0]?.data.length - 2 ?? 0)
    window.addEventListener('resize', drawLines)
    drawLines()
    return () => {
      window.removeEventListener('resize', drawLines)
    }
  }, [data, drawLines, setActiveStepIndex, setDeformationConstructData, status, deformationConstructData])

  useEffect(() => {
    setObjectsArr(warpMarkersObjects)
    setCurrentConstruct(warpMarkersObjects[0])

    return () => {
      setObjectsArr([])
      setCurrentConstruct(null)
    }
  }, [setObjectsArr, setCurrentConstruct])

  useEffect(() => {
    const idFromSearchParams = Number(searchParams.get('id'))
    if (idFromSearchParams) {
      const constructFromSearchParams = warpMarkersObjects.find((el) => el.id === idFromSearchParams)
      if (constructFromSearchParams) {
        setCurrentConstruct(constructFromSearchParams)
      }
    }
  }, [searchParams, setCurrentConstruct])

  return (
    <Layout className={style.container} ref={containerRef} direction='column'>
      {status === 'loading' && !deformationConstructData ? (
        <div className={style.loaderContainer}>
          <Loader />
        </div>
      ) : (
        <>
          {isFetching && (
            <div className={style.fetchLoaderContainer}>
              <Loader />
            </div>
          )}
          <Layout className={style.selectWrapper}>
            <ObjectsSelect />
          </Layout>
          <div className={style.titleContainer}>
            <Card className={style.titleWrapper}>
              <Text size='2xl' weight='bold'>
                {currentConstruct?.label}
              </Text>
            </Card>
          </div>
          {deformationConstructData && deformationConstructData?.deformation_sensors.length > 0 && (
            <>
              <Cube backCanvasRef={backCanvasRef} frontCanvasRef={frontCanvasRef} cubeSideRefsArr={cubeSideRefsArr} />
              <Layout direction='column' className={style.chartsContainer}>
                <ChartsContainers containerRefsArr={containerRefsArr} />
              </Layout>
            </>
          )}
        </>
      )}
    </Layout>
  )
}

export default GeoMonitoring
