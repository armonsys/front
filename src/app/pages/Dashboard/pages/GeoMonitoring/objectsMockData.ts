import { thermometricWellsLayer, warpMarkers } from '@app/pages/Map/layersMockData'
import { Item } from '../../../../components/ObjectsSelect/store/store'

export const thermometricWellsObjects: Item[] = thermometricWellsLayer.areas.map((area) => ({
  label: area.title,
  id: area.id,
}))

export const warpMarkersObjects: Item[] = warpMarkers.areas.map((area) => ({
  label: area.title,
  id: area.id,
}))
