import { createWithEqualityFn } from 'zustand/traditional'
import { DeformationConstructData } from '../api/api'
import { shallow } from 'zustand/shallow'

interface GeoMonitoringState {
  deformationConstructData: DeformationConstructData | undefined
  setDeformationConstructData: (data: DeformationConstructData) => void
  currentDate: string | undefined
  setCurrentDate: (date: string) => void
  activeStepIndex: number
  setActiveStepIndex: (index: number) => void
}

export const useGeoMonitoring = createWithEqualityFn<GeoMonitoringState>(
  (set) => ({
    deformationConstructData: undefined,
    setDeformationConstructData: (data) => set(() => ({ deformationConstructData: data })),
    currentDate: undefined,
    setCurrentDate: (date) => set(() => ({ currentDate: date })),
    activeStepIndex: 0,
    setActiveStepIndex: (index) => set(() => ({ activeStepIndex: index })),
  }),
  shallow,
)
