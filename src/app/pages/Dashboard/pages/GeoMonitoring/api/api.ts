import { apiClient } from '@clients/apiClient'

export type SensorItemData = {
  date_measured: Date
  predicted: boolean
  value: number
}

export type SensorItem = {
  base_point: number
  data: SensorItemData[]
  description: string | null
  id: number
  name: string
}

export type DeformationConstructData = {
  deformation_sensors: SensorItem[]
  id: number
  name: string
  max_move: number
  min_move: number
}
type FetchDeformationSensorsByConstructIdResponse = DeformationConstructData

export const fetchDeformationSensorsByConstructId = async (constructId: number | null) => {
  if (constructId) {
    return (await apiClient
      .get(`constructs/deformation/${constructId}/`)
      .json()) as FetchDeformationSensorsByConstructIdResponse
  }
}
