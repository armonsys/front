import { Layout } from '@consta/uikit/Layout'
import { FC, useMemo } from 'react'

import { useGeoMonitoring } from '../../store/store'
import { ChartContainer } from '../ChartContainer/ChartContainer'
import { Roadmap } from '../Roadmap/Roadmap'
import style from './ChartsContainers.module.scss'

interface IChartsContainersProps {
  containerRefsArr: React.MutableRefObject<HTMLDivElement[]>
}

export const ChartsContainers: FC<IChartsContainersProps> = ({ containerRefsArr }) => {
  const deformationData = useGeoMonitoring((state) => state.deformationConstructData)
  const sensors = useMemo(() => {
    return deformationData?.deformation_sensors ?? []
  }, [deformationData])

  return (
    <>
      <Layout className={style.chartsRowContainer}>
        {sensors[0] && (
          <ChartContainer
            data={sensors[0]}
            containerRef={(el: HTMLDivElement) => {
              containerRefsArr.current[0] = el
            }}
          />
        )}
        {sensors[1] && (
          <ChartContainer
            data={sensors[1]}
            containerRef={(el: HTMLDivElement) => {
              containerRefsArr.current[1] = el
            }}
          />
        )}
      </Layout>
      <Layout className={style.chartsRowContainer}>
        {sensors[2] && (
          <ChartContainer
            data={sensors[2]}
            containerRef={(el: HTMLDivElement) => {
              containerRefsArr.current[2] = el
            }}
          />
        )}
        <div className={style.roadmapContainer}>
          <Roadmap />
        </div>
        {sensors[3] && (
          <ChartContainer
            data={sensors[3]}
            containerRef={(el: HTMLDivElement) => {
              containerRefsArr.current[3] = el
            }}
          />
        )}
      </Layout>
    </>
  )
}
