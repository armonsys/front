import { FC } from 'react'
import style from './Cube.module.scss'

interface ICubeProps {
  backCanvasRef: React.RefObject<HTMLCanvasElement>
  frontCanvasRef: React.RefObject<HTMLCanvasElement>
  cubeSideRefsArr: React.MutableRefObject<HTMLDivElement[]>
}

export const Cube: FC<ICubeProps> = ({ backCanvasRef, frontCanvasRef, cubeSideRefsArr }) => {
  return (
    <>
      <canvas className={style.canvasBack} ref={backCanvasRef} />
      <canvas className={style.canvasFront} ref={frontCanvasRef} />
      <div className={style.cubeContainer}>
        <div className={style.cubeWrapper}>
          <div className={style.cube}>
            <div
              className={style.back}
              ref={(el: HTMLDivElement) => {
                cubeSideRefsArr.current[0] = el
              }}
            />
            <div
              className={style.right}
              ref={(el: HTMLDivElement) => {
                cubeSideRefsArr.current[1] = el
              }}
            />
            <div
              className={style.left}
              ref={(el: HTMLDivElement) => {
                cubeSideRefsArr.current[2] = el
              }}
            />
            <div
              className={style.front}
              ref={(el: HTMLDivElement) => {
                cubeSideRefsArr.current[3] = el
              }}
            />
            <div className={style.top} />
            <div className={style.bottom} />
          </div>
        </div>
      </div>
      <div className={style.tableContainer}>
        <div className={style.table} />
      </div>
    </>
  )
}
