import { FC, useEffect, useState } from 'react'
import style from './Roadmap.module.scss'
import { ProgressStepBar } from '@consta/uikit/ProgressStepBar'
import { useGeoMonitoring } from '../../store/store'

import { IconAlert } from '@consta/icons/IconAlert'

type Item = {
  label: string
  status?: 'normal' | 'success' | 'alert' | 'warning'
  lineStatus?: 'normal' | 'success' | 'alert' | 'warning'
  onClick?: () => void
}

interface IRoadmapProps {}

export const Roadmap: FC<IRoadmapProps> = () => {
  const deformationData = useGeoMonitoring((state) => state.deformationConstructData)
  const [activeStepIndex, setActiveStepIndex] = useGeoMonitoring((state) => [
    state.activeStepIndex,
    state.setActiveStepIndex,
  ])

  const [steps, setSteps] = useState<Item[]>([])
  const [isHide, setIsHide] = useState(false)

  useEffect(() => {
    setIsHide(true)
    if (!deformationData?.deformation_sensors.length) return
    setSteps(
      deformationData?.deformation_sensors[0].data.map((item, idx) => {
        let isWarn: boolean = false
        const max = deformationData?.max_move
        const min = deformationData?.min_move
        const sensors = deformationData?.deformation_sensors
        sensors.forEach((sensor) => {
          const moveSum = sensor.data.slice(0, idx + 1).reduce((acc, item) => +(acc + item.value).toFixed(3), 0)
          if (moveSum <= min || moveSum >= max) {
            isWarn = true
          }
        })
        return {
          label: new Date(item.date_measured).toLocaleDateString('ru-RU'),
          lineStatus: 'normal',
          status: isWarn ? 'alert' : 'normal',
          point: isWarn ? IconAlert : undefined,
          onClick: () => {
            setActiveStepIndex(idx)
          },
        }
      }),
    )
    // hack to fix bug with ProgressStepBar. Because when change steps without rerender, ProgressStepBar not show lines.
    setTimeout(() => setIsHide(false), 0)
  }, [deformationData, setActiveStepIndex])

  return (
    <div className={style.container}>
      {steps.length > 0 && !isHide && (
        <ProgressStepBar className={style.progress} steps={steps} activeStepIndex={activeStepIndex} />
      )}
    </div>
  )
}
