import { Card } from '@consta/uikit/Card'
import { Layout } from '@consta/uikit/Layout'
import { Text } from '@consta/uikit/Text'
import clsx from 'clsx'
import { FC, useCallback, useEffect, useState } from 'react'
import { SensorItem } from '../../api/api'
import { Chart } from '../Chart/Chart'
import style from './ChartContainer.module.scss'
import { useGeoMonitoring } from '../../store/store'

interface IChartContainerProps {
  containerRef: (el: HTMLDivElement) => void
  data: SensorItem
}

export const ChartContainer: FC<IChartContainerProps> = ({ containerRef, data }) => {
  const deformationData = useGeoMonitoring((state) => state.deformationConstructData)
  const activeStepIndex = useGeoMonitoring((state) => state.activeStepIndex)
  const [isWarn, setIsWarn] = useState(false)

  const getIsWarn = useCallback(
    (sensor: SensorItem) => {
      const max = deformationData?.max_move
      const min = deformationData?.min_move
      const sumChange = sensor.data
        .slice(0, activeStepIndex + 1)
        .reduce((acc, item) => +(acc + item.value).toFixed(3), 0)
      return max && min ? sumChange >= max || sumChange <= min : false
    },
    [activeStepIndex, deformationData?.max_move, deformationData?.min_move],
  )

  useEffect(() => {
    setIsWarn(getIsWarn(data))
  }, [data, getIsWarn])

  return (
    <Card data-is-warn={isWarn} className={clsx(style.chartContainer, isWarn && style.warnState)} ref={containerRef}>
      {isWarn && (
        <Layout className={style.warnContainer}>
          <Card className={style.warnWrapper}>
            <Text size='m' weight='bold' className={style.warnText}>
              Превышение порогового значения
            </Text>
            <Text size='m' className={style.warnText}>
              Рекомендована установка дополнительных пологонаклонных (горизонтальных) СОУ при подтверждении
              необходимости и целесообразности теплотехническими расчетами
            </Text>
          </Card>
        </Layout>
      )}
      <Chart data={data} />
    </Card>
  )
}
