import { Waterfall } from '@consta/charts/Waterfall'
import { Layout } from '@consta/uikit/Layout'
import { Text } from '@consta/uikit/Text'
import { FC, useCallback, useEffect, useRef, useState } from 'react'

import { SensorItem } from '../../api/api'
import { useGeoMonitoring } from '../../store/store'
import style from './Chart.module.scss'

type ChartDataItem = {
  date: Date
  value: number
}

interface IChartProps {
  data: SensorItem
}

export const Chart: FC<IChartProps> = ({ data }) => {
  const [activeStepIndex, setActiveStepIndex] = useGeoMonitoring((state) => [
    state.activeStepIndex,
    state.setActiveStepIndex,
  ])
  const deformationConstructData = useGeoMonitoring((state) => state.deformationConstructData)

  const [chartData, setChartData] = useState<ChartDataItem[]>([])
  const [description, setDescription] = useState({
    periodChange: 0,
    sumChange: 0,
    marker: 0,
  })

  const chartContainerRef = useRef<HTMLDivElement>(null)

  const calcBackdrop = () => {
    if (chartContainerRef.current === null) return
    const containerEl = chartContainerRef.current
    const containerWidth = containerEl.getBoundingClientRect().width
    const step = activeStepIndex ?? 0
    const leftPadding = 55
    const rightPadding = 10
    const columnWidth = (containerWidth - leftPadding) / chartData.length
    const start = leftPadding + columnWidth * step
    const end = start + columnWidth - rightPadding
    return `rgb(217 217 217 / 50%) ${start}px, rgb(0 0 0 / 0%) ${start}px, rgb(0 0 0 / 0%) ${end}px, rgb(217 217 217 / 50%) ${end}px`
  }

  const getChartData = (data: SensorItem) => {
    const result = data.data.map((item, idx) => {
      return {
        idx,
        date: item.date_measured,
        value: item.value,
      }
    })
    return result
  }

  const getDescription = useCallback(() => {
    const periodChange = chartData[activeStepIndex]?.value
    const sumChange = Number(
      chartData
        .slice(0, activeStepIndex + 1)
        .reduce((acc, item) => acc + item.value, 0)
        .toFixed(3),
    )
    const marker = Number((data.base_point + sumChange / 1000).toFixed(3))
    return {
      periodChange,
      sumChange,
      marker,
    }
  }, [chartData, data.base_point, activeStepIndex])

  useEffect(() => {
    setChartData(getChartData(data))
  }, [data])

  useEffect(() => {
    setDescription(getDescription())
  }, [getDescription])

  return (
    <Layout direction='column'>
      <Layout className={style.chart} direction='column' ref={chartContainerRef}>
        <Text size='xl' weight='bold'>
          {data.name}
        </Text>
        <div
          className={style.backdrop}
          style={{
            background: `linear-gradient(90deg,${calcBackdrop()})`,
          }}
        />
        <Waterfall
          className={style.waterfallChart}
          tooltip={false}
          labelMode='absolute'
          xField='date'
          yField='value'
          data={chartData}
          legend={false}
          total={false}
          risingFill={'#5081be'}
          fallingFill={'#C00000'}
          state={{
            active: {
              style: {
                cursor: 'pointer',
              },
            },
          }}
          onEvent={(_, ev) => {
            if (ev.type === 'element:click') {
              setActiveStepIndex(ev?.data?.data.idx)
            }
          }}
          yAxis={{
            base: 100,
            min: deformationConstructData?.min_move,
            max: deformationConstructData?.max_move,
            grid: {
              alignTick: true,
              line: {
                style: {
                  stroke: '#d9d9d9',
                  lineWidth: 1,
                  lineDash: [0, 0],
                },
              },
            },
          }}
          xAxis={{
            label: {
              offsetY: 10,
            },
          }}
          meta={{
            value: {
              alias: 'Изменение, мм',
              formatter: (v) => {
                return v.toFixed(2)
              },
            },
            date: {
              formatter: (v) => {
                return new Date(v).toLocaleDateString('ru-RU')
              },
            },
          }}
        />
      </Layout>
      <Layout className={style.descriptionContainer}>
        <Layout className={style.descriptionWrapper} direction='column'>
          <Layout className={style.descriptionItem}>
            <Text className={style.description}>Изменение за период:</Text>{' '}
            <Text className={style.descriptionValue} weight='bold'>
              {description.periodChange} мм
            </Text>
          </Layout>
          <Layout className={style.descriptionItem}>
            <Text className={style.description}>Суммарное изменение:</Text>{' '}
            <Text className={style.descriptionValue} weight='bold'>
              {description.sumChange} мм
            </Text>
          </Layout>
          <Layout className={style.descriptionItem}>
            <Text className={style.description}>Отметка:</Text>{' '}
            <Text className={style.descriptionValue} weight='bold'>
              {description.marker} м
            </Text>
          </Layout>
        </Layout>
      </Layout>
    </Layout>
  )
}
