import { queryClient } from '@clients/queryClient.ts'
import { Theme, presetGpnDefault } from '@consta/uikit/Theme'
import { QueryClientProvider } from '@tanstack/react-query'
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'
import * as React from 'react'
import * as ReactDOM from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import App from './app/App.tsx'
import './index.css'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <ReactQueryDevtools />
      <Theme preset={presetGpnDefault}>
        <BrowserRouter>
          <ToastContainer />
          <App />
        </BrowserRouter>
      </Theme>
    </QueryClientProvider>
  </React.StrictMode>,
)
